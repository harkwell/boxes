Khallware Dots-n-Boxes Game
=================
Overview
---------------
This android application provides a pleasant diversion based on the classic
dots and boxes game; the player with the most squares wins.

```shell
chromium-browser https://play.google.com/store/apps/details?id=com.khallware.boxes&hl=en
chromium-browser https://www.amazon.com/dp/B01MY83T7Z/ref=sr_1_18?s=mobile-apps&ie=UTF8&qid=1484416031&sr=1-18&keywords=boxes
```

First-time Only
---------------

```shell
chromium-browser https://developer.android.com/studio/index.html
# download android studio ide
sudo unzip -d /usr/local android-studio-ide-*-linux.zip
rm android-studio-ide-*-linux.zip
export PATH=$PATH:/usr/local/android-studio/bin/
studio.sh
export PATH=$PATH:$HOME/Android/Sdk/tools/
android
# Android SDK build-tools v22.0.1
# Android 5.1.1 - SDK Platform v2
# Android 5.1.1 - Google APIs v1
# Android 5.1.1 - Intel x86 Atom_64 System Image
# accept license and install
android update sdk --no-ui --obsolete --force
```

If you are running under centos7 minimal and have the required dependencies
already, ignore the following:

```shell
docker run -it -h build-boxes --name build-boxes -v $HOME/Android:/usr/local/Android centos
yum install -y git maven-3.0.5 java-1.8.0-openjdk-devel
#yum install -y epel-release
#yum install -y git zlib.i686 ncurses-libs.i686 bzip2-libs.i686
mkdir -p ~/.m2/
cat <<'EOF' >~/.m2/settings.xml
<settings xmlns="http://maven.apache.org/POM/4.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
      http://maven.apache.org/xsd/settings-1.0.0.xsd">
   <profiles>
      <profile>
         <id>boxes-properties</id>
         <properties>
            <android.sdk.path>/usr/local/Android/Sdk</android.sdk.path>
         </properties>
      </profile>
   </profiles>
   <activeProfiles>
      <activeProfile>boxes-properties</activeProfile>
   </activeProfiles>
</settings>
EOF
```

Build
---------------

```shell
git clone https://gitlab.com/harkwell/boxes.git && cd boxes
export ANDROID_HOME=/home/khall/Android/Sdk/
cd boxes-lib && mvn package
mvn install:install-file -Dfile=target/Boxes.jar -DgroupId=com.khallware.boxes -DartifactId=khallware-boxes -Dversion=0.2 -Dpackaging=jar
cd .. && mvn package
ls -ld target/Boxes.apk
```

Deploy
---------------

```shell
# first time
android create avd -n fovea -t 26
android list targets
mksdcard 256M ~/tmp/sdcard1.iso
emulator -sdcard ~/tmp/sdcard1.iso -avd fovea
# on phones, don't forget to set "settings" -> "security" -> "unknown sources"

# re-install
adb uninstall com.khallware.boxes
adb install target/Boxes.apk
```


Releases
---------------

```shell
# tag release (eg "boxes_1_1")
# checkout source from tag

git checkout boxes_1_1
SDKVER=$(android list targets |grep ^id: |tail -1 |awk '{print $2}')
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++))

# release apk
PASS=MYPASSWORD
cp keystore.jks /tmp/
mvn package -Dbuild.apkver=$(date +%Y%m%d) -Dbuild.sdkver=$SDKVER -Dbuild.incr=$(date +%Y%m%d)-$((count++)) -Dandroid.release=true -Dkeystore.password=$PASS -Dsigning-keystore-alias=khallware.com -Psign
```

Testing
---------------

```shell
adb uninstall com.khallware.boxes
adb install boxes.apk
adb shell monkey -v -p com.khallware.boxes 500
```
