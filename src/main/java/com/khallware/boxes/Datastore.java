/** $Header: /home/khall/cvs/boxes/gui/src/net/dnsalias/hall/boxes/Datastore.java,v 1.6 2013/09/07 11:50:08 khall Exp $
 * ============================================================================
 * Datastore.java is a helper class for dealing with stored data
 *
 * The Android "Boxes" application
 * (C) 2013-2016 Kevin D.Hall
 *
 * The software, processes, trade secrets and technical/business know-how used
 * on these premises are the property of Kevin D.Hall and are not to be copied,  * divulged or used without the express written consent of the author.
 */
package com.khallware.boxes;

import com.khallware.boxes.Config.GameMode;
import com.khallware.boxes.chooser.Chooser;
import com.khallware.boxes.chooser.Util;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteException;
import android.database.DatabaseErrorHandler;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Datastore extends SQLiteOpenHelper
{
	public static final String DB_FILE = "boxes.db";
	public static final String DB_PATH = "/data/data/com.khallware"
		+".boxes/databases/";
	public static final int DB_VERSION = 1;
	public static final String DELIMETER = ",";
	public static final int BOARD_ID = 1;
	public static final int GAME_ID = 1;
	public static final int UNKNOWN = -1;
	private static Datastore instance = null;

	private Logger logger = LoggerFactory.getLogger(Datastore.class);
	private DateFormat df = new SimpleDateFormat(Constants.DATE_FORMAT);
	private SQLiteDatabase handle = null;

	private Datastore(Context ctxt, SharedPreferences prefs)
	{
		super(ctxt, DB_FILE, null, DB_VERSION,
				new DatabaseErrorHandler() {
			public void onCorruption(SQLiteDatabase dbObj) {
			}
		});
	}

	public static Datastore getDatastore()
	{
		return(instance);
	}

	public static Datastore getDatastore(Context ctxt,
			SharedPreferences prefs)
	{
		if (instance != null) {
			return(instance);
		}
		instance = new Datastore(ctxt, prefs);
		return(instance);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		logger.debug("onCreate(SQLiteDatabase)ing...");
		String sql = "CREATE TABLE board ("
			+           "id INTEGER NOT NULL, "
			+           "width INTEGER NOT NULL, "
			+           "height INTEGER NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE game ("
			+           "id INTEGER NOT NULL, "
			+           "board_id INTEGER NOT NULL, "
			+           "player INTEGER NOT NULL, "
			+           "point1 INTEGER NOT NULL, "
			+           "point2 INTEGER NOT NULL, "
			+           "date DATE NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE player ("
			+           "id INTEGER NOT NULL, "
			+           "name VARCHAR(255) NOT NULL, "
			+           "chooser VARCHAR(255) NOT NULL, "
			+           "color INTEGER NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE config ("
			+           "mode VARCHAR(255) NOT NULL, "
			+           "theme INTEGER NOT NULL, "
			+           "doAnimTrail VARCHAR(10) NOT NULL, "
			+           "doDrawTrail VARCHAR(10) NOT NULL, "
			+           "doVibrate VARCHAR(10) NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
		sql = "CREATE TABLE state ("
			+           "playersTurn INTEGER NOT NULL"
			+    ")";
		logger.debug("sql: " +sql);
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer)
	{
		logger.debug("onUpgrade()ing... from "+oldVer+" to "+newVer);
		onCreate(db);
	}

	public void prepForNewGame() throws DatastoreException
	{
		try {
			prepForNewGameUnwrapped(GAME_ID, BOARD_ID);
		}
		catch (SQLiteException e) {
			throw new DatastoreException(e);
		}
	}

	public void saveState(Player player)
	{
		String sql = "DELETE FROM state";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);

		sql = "INSERT INTO state (playersTurn) "
			+         "VALUES ("
			+                 player.getId()
			+         ")";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	public void saveConfig()
	{
		String sql = "DELETE FROM config";
		Config cfg = Config.getConfig();
		logger.debug("sql: " +sql);
		handle().execSQL(sql);

		sql = "INSERT INTO config (mode, theme, doAnimTrail, "
			+                 "doDrawTrail, doVibrate) "
			+         "VALUES ("
			+             "'"+cfg.getMode()+"', "
			+                 cfg.getThemeIndex()+", "
			+             "'"+cfg.doAnimTrail()+"', "
			+             "'"+cfg.doDrawTrail()+"', "
			+             "'"+cfg.doVibrate()+"'"
			+         ")";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	/**
	 * Load the game configuration from the database.
	 */
	public void loadConfig()
	{
		Cursor cursor = null;
		Config cfg = Config.getConfig();
		String sql = "SELECT mode, theme, doAnimTrail, doDrawTrail, "
			+           "doVibrate "
			+      "FROM config";
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		if (cursor.moveToNext()) {
			cfg.setMode(GameMode.valueOf(cursor.getString(
				cursor.getColumnIndexOrThrow("mode"))));
			cfg.setThemeIndex(cursor.getInt(
				cursor.getColumnIndexOrThrow("theme")));
			cfg.setAnimTrail(Boolean.parseBoolean(cursor.getString(
				cursor.getColumnIndexOrThrow("doAnimTrail"))));
			cfg.setDrawTrail(Boolean.parseBoolean(cursor.getString(
				cursor.getColumnIndexOrThrow("doDrawTrail"))));
			cfg.setVibrate(Boolean.parseBoolean(cursor.getString(
				cursor.getColumnIndexOrThrow("doVibrate"))));
		}
		cursor.close();
	}

	/**
	 * Determine which players turn it is...
	 */
	public int playersTurn()
	{
		int retval = UNKNOWN;
		Cursor cursor = null;
		String sql = "SELECT playersTurn FROM state";
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		if (cursor.moveToNext()) {
			retval = cursor.getInt(
				cursor.getColumnIndexOrThrow("playersTurn"));
		}
		cursor.close();
		return(retval);
	}

	public void truncatePlayers() throws DatastoreException
	{
		try {
			truncatePlayersUnwrapped();
		}
		catch (SQLiteException e) {
			throw new DatastoreException(e);
		}
	}

	public List<Player> getPlayers() throws DatastoreException
	{
		return(getPlayers(GAME_ID));
	}

	public List<Player> getPlayers(int game) throws DatastoreException
	{
		try {
			return(getPlayersUnwrapped(game));
		}
		catch (SQLiteException e) {
			throw new DatastoreException(e);
		}
	}

	public void setPlayer(Player player) throws DatastoreException
	{
		setPlayer(player, new GfxChooser());
	}

	public void setPlayer(Player player, Chooser chooser)
			throws DatastoreException
	{
		try {
			setPlayerUnwrapped(player, chooser);
		}
		catch (SQLiteException e) {
			throw new DatastoreException(e);
		}
	}

	public GfxGameBoard loadGameBoard() throws DatastoreException
	{
		logger.debug("loadGameBoard()ing...");
		try {
			return(loadGameBoardUnwrapped(GAME_ID, BOARD_ID));
		}
		catch (Exception e) {
			if (e instanceof DatastoreException) {
				throw (DatastoreException)e;
			}
			else {
				throw new DatastoreException(e);
			}
		}
	}

	public void save(GfxGameBoard board) throws DatastoreException
	{
		logger.debug("save(GfxGameBoard)ing...");
		try {
			saveUnwrapped(board, GAME_ID, BOARD_ID);
		}
		catch (Exception e) {
			if (e instanceof DatastoreException) {
				throw (DatastoreException)e;
			}
			else {
				throw new DatastoreException(e);
			}
		}
	}

	public void saveMove(int player, int point1, int point2)
			throws DatastoreException
	{
		saveMove(GAME_ID, BOARD_ID, player, new Date(), point1, point2);
	}

	public void saveMove(int game, int board, int player,
			Date date, int point1, int point2)
			throws DatastoreException
	{
		try {
			saveMoveUnwrapped(game, board, player, date, point1,
				point2);
		}
		catch (SQLiteException e) {
			throw new DatastoreException(e);
		}
	}

	private GfxGameBoard loadGameBoardUnwrapped(int gameId, int boardId)
			throws SQLiteException, GameException,
			DatastoreException
	{
		GfxGameBoard retval = null;
		Cursor cursor = null;
		Player player = null;
		int count = 0;
		int[] boardSize = getBoardSize(boardId);
		List<Player> players = getPlayers(gameId);
		String sql = "SELECT player, point1, point2 "
			+      "FROM game "
			+     "WHERE id = "+gameId+" "
			+       "AND board_id = "+boardId;
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});
		retval = new GfxGameBoard(boardSize[0], boardSize[1], players);

		while (cursor.moveToNext()) {
			int playerId = cursor.getInt(
				cursor.getColumnIndexOrThrow("player"));
			int pt1 = cursor.getInt(
				cursor.getColumnIndexOrThrow("point1"));
			int pt2 = cursor.getInt(
				cursor.getColumnIndexOrThrow("point2"));
			Point point1 = new GamePoint(pt1,
				GamePoint.MAX_REFERENCES);
			Point point2 = new GamePoint(pt2,
				GamePoint.MAX_REFERENCES);
			Edge edge = Util.findEdge(new GameEdge(point1, point2),
				retval.getSquares());
			count++;

			if ((player = findPlayer(playerId, players)) != null) {
				retval.move(player, edge);
				Util.increaseScore(retval, player);
			}
			else {
				logger.error("could not find player "+playerId);
			}
		}
		cursor.close();
		logger.debug("found "+count+" taken edges on saved board");
		return(retval);
	}

	private static GfxGamePlayer findPlayer(int id, List<Player> players)
	{
		GfxGamePlayer retval = null;

		for (Player player : players) {
			if (player.getId() == id) {
				retval = (GfxGamePlayer)player;
				break;
			}
		}
		return(retval);
	}

	/**
	 * Ignore the game id in this current implementation. YAGNI!
	 */
	private List<Player> getPlayersUnwrapped(int game)
			throws SQLiteException
	{
		List<Player> retval = Config.getConfig().getPlayers();

		/*  The players are stored in the Config singleton!
		List<Player> retval = new ArrayList<>();
		Cursor cursor = null;
		String sql = "SELECT id, name, color "
			+      "FROM player";
		int count = 0;
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		while (cursor.moveToNext()) {
			int id = cursor.getInt(
				cursor.getColumnIndexOrThrow("id"));
			String name = cursor.getString(
				cursor.getColumnIndexOrThrow("name"));
			int color = cursor.getInt(
				cursor.getColumnIndexOrThrow("color"));
			GfxGamePlayer player = new GfxGamePlayer(id, name);
			player.setColor(color);
			retval.add(player);
			count++;
		}
		cursor.close();
		logger.debug("loaded "+count+" players");
		//KDH what about the choosers!
		*/
		logger.debug("loaded "+retval.size()+" players");
		return(retval);
	}

	private void setPlayerUnwrapped(Player player, Chooser chooser)
			throws SQLiteException
	{
		GfxGamePlayer p = (GfxGamePlayer)player;
		String sql = "INSERT INTO player (id, name, chooser, color) "
			+         "VALUES ("
			+                 p.getId()+", "
			+             "'"+p.getName()+"', "
			+             "'"+chooser.getClass().getName()+"', "
			+                 p.getColor()
			+         ")";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	private int[] getBoardSize(int board) throws SQLiteException
	{
		int[] retval = new int[] { Constants.DEF_BOARD_WIDTH,
			Constants.DEF_BOARD_HEIGHT };
		Cursor cursor = null;
		String sql = "SELECT width, height "
			+      "FROM board "
			+     "WHERE id = "+board;
		logger.debug("sql: " +sql);
		cursor = handle().rawQuery(sql, new String[] {});

		if (cursor.moveToNext()) {
			retval[0] = cursor.getInt(
				cursor.getColumnIndexOrThrow("width"));
			retval[1] = cursor.getInt(
				cursor.getColumnIndexOrThrow("height"));
		}
		cursor.close();
		return(retval);
	}

	private void saveUnwrapped(GfxGameBoard board, int gameId, int boardId)
			throws SQLiteException, DatastoreException
	{
		Date now = new Date();
		int count = 0;
		prepForNewGame();

		for (Player player : board.getPlayers()) {
			for (Square sq : ((GamePlayer)player).getSquares()) {
				for (Edge edge : ((GameSquare)sq).getEdges()) {
					saveMove(gameId, boardId,
						player.getId(), now,
						edge.getPoint1().getId(),
						edge.getPoint2().getId());
					count++;
				}
			}
		}
		logger.debug("saved "+count+" edges of the board");
	}

	private void truncatePlayersUnwrapped() throws SQLiteException
	{
		String sql = "DELETE FROM player";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	private void prepForNewGameUnwrapped(int gameId, int boardId)
			throws SQLiteException
	{
		Config cfg = Config.getConfig();
		String sql = "DELETE FROM game WHERE id = "+gameId;
		logger.debug("sql: " +sql);
		handle().execSQL(sql);

		sql = "DELETE FROM board WHERE id = "+boardId;
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}

	private void saveMoveUnwrapped(int game, int board, int player,
			Date date, int point1, int point2)
			throws SQLiteException
	{
		String sql = "INSERT INTO game (id, board_id, date, "
			+                "player, point1, point2) "
			+         "VALUES ("
			+                 game+", "
			+                 board+", "
			+                 df.format(date)+", "
			+             "'"+player+"', "
			+             "'"+point1+"', "
			+             "'"+point2+"'"
			+         ")";
		logger.debug("sql: " +sql);
		handle().execSQL(sql);
	}


	private SQLiteDatabase handle()
	{
		if (handle == null || !handle.isOpen()) {
			handle = getWritableDatabase();
		}
		return(handle);
	}
}
