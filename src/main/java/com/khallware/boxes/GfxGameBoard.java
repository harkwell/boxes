// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.theme.Theme;
import com.khallware.boxes.theme.Theme.Item;
import com.khallware.boxes.chooser.Util;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GfxGameBoard extends GameBoard implements Graphical
{
	private static final Logger logger =
		LoggerFactory.getLogger(GfxGameBoard.class);
	private Paint paint = new Paint();
	private Rect surfaceRect = null;

	public GfxGameBoard(int width, int height, List<Player> players)
			throws GameException
	{
		super(width, height, players);
	}

	@Override
	public Point newPoint(int id, int maxRefs)
	{
		return(new GfxGamePoint(id, maxRefs));
	}

	@Override
	public Square newSquare(Point p1, Point p2, Point p3, Point p4)
	{
		List<Edge> list = Util.createOrFindEdges(this, p1, p2, p3, p4);
		List<Edge> removeList = new ArrayList<>();

		for (Edge edge : list) {
			if (edge instanceof GfxGameEdge) {
				continue;
			}
			removeList.add(edge);
		}
		for (Edge edge : removeList) {
			list.remove(edge);
			list.add(new GfxGameEdge(edge));
		}
		return(new GfxGameSquare(list));
	}

	/**
	 * Resize the contained board.  Set the physical locations of the
	 * existing points in the space.
	 */
	public void resize(Rect surfaceRect)
	{
		int boardWidth = surfaceRect.width();
		int boardHeight = surfaceRect.height();
		int numRows = getPointGrid()[0].length;
		int numCols = -1;
		int leftMargin = -1;
		int topMargin = ((boardHeight / numRows) / 2);
		int xCoord = 0;
		int yCoord = 0;
		int rowCount = 0;
		int colCount = 0;
		this.surfaceRect = surfaceRect;
		logger.info("resizing board to "+boardWidth+"x"+boardHeight);

		for (Point[] row : getPointGrid()) {
			if (boardWidth <= 0 || boardHeight <= 0) {
				logger.error("ignoring invalid board size!");
				break;
			}
			numCols = row.length;
			leftMargin = ((boardWidth / numCols) / 2);
			xCoord = leftMargin;
			yCoord = topMargin;
			colCount = 0;
			logger.debug("board: num_rows="+numRows+" "
				+"num_cols="+numCols);

			for (Point point : row) {
				xCoord = leftMargin + (colCount
					* (boardWidth / numCols));
				yCoord = topMargin + (rowCount
					* (boardHeight / numRows));
				colCount++;
				((GfxGamePoint)point).setXCoord(xCoord);
				((GfxGamePoint)point).setYCoord(yCoord);
				logger.debug("set point ("+point+")");
			}
			rowCount++;
		}
	}

	public void draw(Canvas canvas) throws GameException
	{
		Theme theme = Config.getConfig().getTheme();
		boolean valid = (theme.getBitmap(Item.background) != null);
		valid &= (surfaceRect != null);

		if (valid) {
			canvas.drawBitmap(
				theme.getBitmap(Item.background),
				null, surfaceRect, null);
		}
		for (Edge edge : getEdges()) {
			if (!(edge instanceof Graphical)) {
				continue;
			}
			((Graphical)edge).draw(canvas);

		}
		for (Square square : getSquares()) {
			if (!(square instanceof Graphical)) {
				continue;
			}
			((Graphical)square).draw(canvas);
		}
		for (Point point : getPoints()) {
			if (!(point instanceof Graphical)) {
				continue;
			}
			((Graphical)point).draw(canvas);

		}
		for (Player player : getPlayers()) {
			if (!(player instanceof Graphical)) {
				continue;
			}
			((Graphical)player).draw(canvas);
		}
	}
}
