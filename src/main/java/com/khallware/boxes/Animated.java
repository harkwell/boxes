// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.graphics.Canvas;

public interface Animated extends Graphical
{
	public static final long DEF_ANIM_DURATION = (2 * 1000);

	public boolean isExpired();
	public boolean isReadyToAnimate();
	public void animate(Canvas canvas);
	public void startAnimation();
}
