// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import android.content.res.Resources;
import android.graphics.Bitmap;

public interface Theme
{
	public static enum Item {
		background,
		takenEdge,
		untakenEdge,
		selectedPoint,
		unselectedPoint,
		square,
		CPU,
		player1,
		player2,
		player3,
		sparkle
	};
	public void setResources(Resources resources);
	public Bitmap getBitmap(Item item);
	public int getColor(Item item);
	public int getOpacity(Item item);
}
