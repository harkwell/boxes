// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public class Logo extends AbstractTheme
{
	private static Map<Item, Integer> cMap = new Hashtable<>();
	private static Map<Item, Integer> oMap = new Hashtable<>();
	private static Map<Item, Integer> bMap = new Hashtable<>();

	static {
		cMap.put(Item.unselectedPoint, Color.parseColor("#f6961a"));
		cMap.put(Item.untakenEdge,     Color.parseColor("#995d10"));
		cMap.put(Item.takenEdge,       Color.parseColor("#f6961a"));
		cMap.put(Item.sparkle,         Color.parseColor("#ffffff"));
		cMap.put(Item.CPU,             Color.parseColor("#060401"));
		cMap.put(Item.player1,         Color.parseColor("#f0f0f0"));
		cMap.put(Item.player2,         Color.parseColor("#b78354"));
		cMap.put(Item.player3,         Color.parseColor("#392192"));
		cMap.put(Item.selectedPoint,   Color.parseColor("#f7e618"));

		// opacity [0..255] 0=transparent 255=opaque
		oMap.put(Item.square,          150);

		// bitmap resource map
		bMap.put(Item.background,      R.drawable.boxes);
	}

	public Logo(Resources resources)
	{
		super(resources);
	}

	protected Map<Item, Integer> getColorMap()
	{
		return(cMap);
	}

	protected Map<Item, Integer> getBitmapMap()
	{
		return(bMap);
	}

	protected Map<Item, Integer> getOpacityMap()
	{
		return(oMap);
	}
}
