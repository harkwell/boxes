// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import com.khallware.boxes.Constants;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public abstract class AbstractTheme implements Theme
{
	public static final int DEFAULT_COLOR = Color.parseColor("#51748e");
	private Resources resources = null;

	public AbstractTheme(Resources resources)
	{
		setResources(resources);
	}

	protected abstract Map<Item, Integer> getColorMap();
	protected abstract Map<Item, Integer> getBitmapMap();
	protected abstract Map<Item, Integer> getOpacityMap();

	public void setResources(Resources resources)
	{
		this.resources = resources;
	}

	/*
	 * Get the bitmap for the given item.  May return null.
	 */
	public Bitmap getBitmap(Item item)
	{
		Bitmap retval = null;

		if (getBitmapMap().containsKey(item)) {
			retval = BitmapFactory.decodeResource(resources,
				getBitmapMap().get(item));
		}
		return(retval);
	}

	public int getColor(Item item)
	{
		return((getColorMap().containsKey(item))
			? getColorMap().get(item)
			: DEFAULT_COLOR);
	}

	public int getOpacity(Item item)
	{
		return((getOpacityMap().containsKey(item))
			? getOpacityMap().get(item)
			: Constants.DEF_OPACITY);
	}
}
