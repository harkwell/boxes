// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public class Blue extends AbstractTheme
{
	private static Map<Item, Integer> cMap = new Hashtable<>();
	private static Map<Item, Integer> oMap = new Hashtable<>();
	private static Map<Item, Integer> bMap = new Hashtable<>();

	static {
		cMap.put(Item.unselectedPoint, Color.parseColor("#159615"));
		cMap.put(Item.untakenEdge,     Color.parseColor("#000000"));
		cMap.put(Item.takenEdge,       Color.parseColor("#eff623"));
		cMap.put(Item.sparkle,         Color.parseColor("#f7e618"));
		cMap.put(Item.CPU,             Color.parseColor("#f7a718"));
		cMap.put(Item.player1,         Color.parseColor("#91d1ff"));
		cMap.put(Item.player2,         Color.parseColor("#f7a7cc"));
		cMap.put(Item.player3,         Color.parseColor("#ef1323"));
		cMap.put(Item.selectedPoint,   Color.parseColor("#962d15"));

		// opacity [0..255] 0=transparent 255=opaque
		oMap.put(Item.square,          150);

		// bitmap resource map
		bMap.put(Item.background,      R.drawable.blue);
		bMap.put(Item.unselectedPoint, R.drawable.gnuhoof);
	}

	public Blue(Resources resources)
	{
		super(resources);
	}

	protected Map<Item, Integer> getColorMap()
	{
		return(cMap);
	}

	protected Map<Item, Integer> getBitmapMap()
	{
		return(bMap);
	}

	protected Map<Item, Integer> getOpacityMap()
	{
		return(oMap);
	}
}
