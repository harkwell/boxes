// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public class Samuel extends AbstractTheme
{
	private static Map<Item, Integer> cMap = new Hashtable<>();
	private static Map<Item, Integer> oMap = new Hashtable<>();
	private static Map<Item, Integer> bMap = new Hashtable<>();

	static {
		cMap.put(Item.unselectedPoint, Color.parseColor("#000000"));
		cMap.put(Item.untakenEdge,     Color.parseColor("#000000"));
		cMap.put(Item.takenEdge,       Color.parseColor("#ffbd00"));
		cMap.put(Item.sparkle,         Color.parseColor("#ffbd00"));
		cMap.put(Item.CPU,             Color.parseColor("#e02845"));
		cMap.put(Item.player1,         Color.parseColor("#3bab4b"));
		cMap.put(Item.player2,         Color.parseColor("#371f7e"));
		cMap.put(Item.player3,         Color.parseColor("#0ebf90"));
		cMap.put(Item.selectedPoint,   Color.parseColor("#ffffff"));

		// opacity [0..255] 0=transparent 255=opaque
		oMap.put(Item.square,          255);

		// bitmap resource map
		bMap.put(Item.background,      R.drawable.slh);
	}

	public Samuel(Resources resources)
	{
		super(resources);
	}

	protected Map<Item, Integer> getColorMap()
	{
		return(cMap);
	}

	protected Map<Item, Integer> getBitmapMap()
	{
		return(bMap);
	}

	protected Map<Item, Integer> getOpacityMap()
	{
		return(oMap);
	}
}
