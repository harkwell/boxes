// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public class Android extends AbstractTheme
{
	private static Map<Item, Integer> cMap = new Hashtable<>();
	private static Map<Item, Integer> oMap = new Hashtable<>();
	private static Map<Item, Integer> bMap = new Hashtable<>();

	static {
		cMap.put(Item.unselectedPoint, Color.parseColor("#eff72a"));
		cMap.put(Item.untakenEdge,     Color.parseColor("#000000"));
		cMap.put(Item.takenEdge,       Color.parseColor("#d0ff47"));
		cMap.put(Item.sparkle,         Color.parseColor("#000000"));
		cMap.put(Item.CPU,             Color.parseColor("#2ba9ff"));
		cMap.put(Item.player1,         Color.parseColor("#f6ff2b"));
		cMap.put(Item.player2,         Color.parseColor("#2ba9ff"));
		cMap.put(Item.player3,         Color.parseColor("#ff9c1b"));
		cMap.put(Item.selectedPoint,   Color.parseColor("#000000"));

		// opacity [0..255] 0=transparent 255=opaque
		oMap.put(Item.square,          255);

		// bitmap resource map
		bMap.put(Item.background,      R.drawable.android);
	}

	public Android(Resources resources)
	{
		super(resources);
	}

	protected Map<Item, Integer> getColorMap()
	{
		return(cMap);
	}

	protected Map<Item, Integer> getBitmapMap()
	{
		return(bMap);
	}

	protected Map<Item, Integer> getOpacityMap()
	{
		return(oMap);
	}
}
