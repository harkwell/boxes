// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.theme;

import com.khallware.boxes.R;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.Hashtable;
import java.util.Map;

public class Tux extends AbstractTheme
{
	private static Map<Item, Integer> cMap = new Hashtable<>();
	private static Map<Item, Integer> oMap = new Hashtable<>();
	private static Map<Item, Integer> bMap = new Hashtable<>();

	static {
		cMap.put(Item.untakenEdge,     Color.parseColor("#000000"));
		cMap.put(Item.takenEdge,       Color.parseColor("#fbff0c"));
		cMap.put(Item.sparkle,	       Color.parseColor("#0cfdff"));
		cMap.put(Item.CPU,	       Color.parseColor("#b400ed"));
		cMap.put(Item.player1,	       Color.parseColor("#7fe547"));
		cMap.put(Item.player2,	       Color.parseColor("#51748e"));
		cMap.put(Item.player3,	       Color.parseColor("#f7a7cc"));
		cMap.put(Item.selectedPoint,   Color.parseColor("#0cfdff"));

		// opacity [0..255] 0=transparent 255=opaque
		oMap.put(Item.square,          255);
		oMap.put(Item.unselectedPoint, 0);
		oMap.put(Item.selectedPoint,   100);

		// bitmap resource map
		bMap.put(Item.background,      R.drawable.tux);
	}

	public Tux(Resources resources)
	{
		super(resources);
	}

	protected Map<Item, Integer> getColorMap()
	{
		return(cMap);
	}

	protected Map<Item, Integer> getBitmapMap()
	{
		return(bMap);
	}

	protected Map<Item, Integer> getOpacityMap()
	{
		return(oMap);
	}
}
