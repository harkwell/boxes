// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.content.Context;
import android.widget.TextView;
import android.os.Bundle;
import android.app.Dialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.SurfaceView;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Boxes extends Activity
{
	private static final Logger logger = LoggerFactory.getLogger(
		Boxes.class);
	private Config cfg = Config.getConfig();
	private Dialog aboutDialog = null;
	private Datastore dstore = null;

	@Override
	public void onCreate(Bundle bundle)
	{
		int mode = Context.MODE_PRIVATE;
		dstore = Datastore.getDatastore(this, getPreferences(mode));
		cfg.loadThemes(getResources());
		cfg.setModeToHard(); // bugfix: factory defaults override
		super.onCreate(bundle);
		try {
			setContentView(new GfxSurfaceView(this));
		}
		catch (GameException e) {
			logger.error(""+e, e);
		}
		getAboutDialog(this);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		cfg.loadThemes(getResources());
		dstore.loadConfig();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.boxes_menu, menu);
		return(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		try {
			handleOption(item.getItemId());
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
		return(true);
	}

	private void handleOption(int option) throws DatastoreException
	{
		ThreadManager mgr = ThreadManager.getThreadManager();
		Datastore dstore = Datastore.getDatastore();
		Config cfg = Config.getConfig();

		switch (option) {
	        case R.id.cpu_easy:
			cfg.setModeToEasy();
			dstore.saveConfig();
			handleOption(R.id.restart);
			break;
	        case R.id.cpu_moderate:
			cfg.setModeToModerate();
			dstore.saveConfig();
			handleOption(R.id.restart);
			break;
	        case R.id.cpu_hard:
			cfg.setModeToHard();
			dstore.saveConfig();
			handleOption(R.id.restart);
			break;
	        case R.id.two_player:
			cfg.setModeToTwoPlayer();
			dstore.saveConfig();
			handleOption(R.id.restart);
			break;
	        case R.id.three_player:
			cfg.setModeToThreePlayer();
			dstore.saveConfig();
			handleOption(R.id.restart);
			break;
	        case R.id.restart:
			dstore.prepForNewGame();
			mgr.stopGame();
			recreate();
			break;
	        case R.id.toggle_vibrate:
			cfg.setVibrate(!cfg.doVibrate());
			break;
	        case R.id.toggle_sparkle:
			cfg.setAnimTrail(!cfg.doAnimTrail());
			break;
	        case R.id.toggle_nextplayer:
			cfg.setNotify(!cfg.doNotify());
			break;
	        case R.id.about:
			getAboutDialog(this).show();
			break;
		}
	}

	private Dialog getAboutDialog(Context context)
	{
		if (aboutDialog != null) {
			return(aboutDialog);
		}
		Builder builder = new Builder(context);
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = null;
		try {
			view = inflater.inflate(R.layout.about, null, false);
		}
		catch (InflateException e) {
			view = new TextView(context);
			((TextView)view).setText(""+e);
			view.setClickable(false);
		}
		builder.setMessage(R.string.lit_about);
		builder.setView(view);
		aboutDialog = builder.create();
		return(aboutDialog);
	}
}
