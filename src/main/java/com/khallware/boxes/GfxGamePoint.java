// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.theme.Theme;
import com.khallware.boxes.theme.Theme.Item;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Paint;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class GfxGamePoint extends GamePoint implements Graphical
{
	private static final Logger logger = LoggerFactory.getLogger(
		GfxGamePoint.class);

	protected int xCoord = 0;
	protected int yCoord = 0;
	protected boolean animate = false;
	protected boolean isSelected = false;
	protected List<SelectHandler> handlers = null;
	protected Config cfg = Config.getConfig();
	protected Paint paint = new Paint();

	public interface SelectHandler
	{
		public void onSelection(boolean selected);
	}

	public GfxGamePoint(int id, int numReferences)
	{
		super(id, numReferences);
		handlers = new ArrayList<>();
	}

	public void setXCoord(int xCoord)
	{
		this.xCoord = xCoord;
	}

	public int getXCoord()
	{
		return(xCoord);
	}

	public void setYCoord(int yCoord)
	{
		this.yCoord = yCoord;
	}

	public int getYCoord()
	{
		return(yCoord);
	}

	public void doAnimate(boolean animate)
	{
		this.animate = animate;
	}

	public void addSelectHandler(SelectHandler handler)
	{
		if (!handlers.contains(handler)) {
			logger.debug("addSelectHandler()...");
			handlers.add(handler);
		}
	}

	public void select(boolean isSelected)
	{
		this.isSelected = isSelected;
		logger.debug(((isSelected) ? "" : "un")+"selected...");
		doAnimate(isSelected);

		for (SelectHandler handler : handlers) {
			handler.onSelection(isSelected);
		}
	}

	public boolean isSelected()
	{
		return(isSelected);
	}

	public void draw(Canvas canvas) throws GameException
	{
		float radius = 10;
		Theme theme = cfg.getTheme();
		Bitmap bitmap = theme.getBitmap(Item.unselectedPoint);
		int opacity = theme.getOpacity(Item.unselectedPoint);
		int color = (isSelected())
			? theme.getColor(Item.selectedPoint)
			: theme.getColor(Item.unselectedPoint);
		paint.setColor(color);
		paint.setStyle(Paint.Style.FILL);
		paint.setAlpha(opacity);

		if (hasAvailableReference()) {
			if (bitmap == null) {
				canvas.drawCircle(getXCoord(), getYCoord(),
					radius, paint);
			}
			else {
				int left = getXCoord()-(bitmap.getWidth() / 2);
				int top = getYCoord()-(bitmap.getHeight() / 2);
				canvas.drawBitmap(bitmap, left, top, paint);
			}
		}
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(super.toString()+" ")
			.append("xCoord="+getXCoord()+" ")
			.append("yCoord="+getYCoord()+" ")
			.append("animate="+animate+" ")
			.append("isSelected="+isSelected()+" ")
			.append("num_handlers="+handlers.size())
			.toString());
	}
}
