// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GfxUtil
{
	private static final Logger logger = LoggerFactory.getLogger(
		GfxUtil.class);

	public static List<Animated> findAnimated(GfxGameBoard board)
	{
		List<Animated> retval = new ArrayList<>();

		if (board instanceof Animated) {
			retval.add((Animated)board);
		}
		for (Edge edge : board.getEdges()) {
			if (edge instanceof Animated) {
				retval.add((Animated)edge);
			}

		}
		for (Square square : board.getSquares()) {
			if (square instanceof Animated) {
				retval.add((Animated)square);
			}
		}
		for (Point point : board.getPoints()) {
			if (point instanceof Animated) {
				retval.add((Animated)point);
			}

		}
		for (Player player : board.getPlayers()) {
			if (player instanceof Animated) {
				retval.add((Animated)player);
			}
		}
		return(retval);
	}

	public static void displayMessage(final String message,
			final Paint paint, final long millis,
			final boolean fullscreen, final boolean animate)
	{
		final Date expiration = new Date(new Date().getTime() + millis);
		ThreadManager.getThreadManager().enqueue(new Animated() {
			private int opacity = 100;
			public boolean isExpired()
			{
				boolean retval = expiration.before(new Date());
				/* logger.debug("KDH: expires: "+expiration);
				logger.debug("KDH: has "+((retval)?"":"not ")
					+"expired on "+new Date()); */
				return(retval);
			}
			public boolean isReadyToAnimate()
			{
				return(!isExpired());
			}
			public void draw(Canvas canvas)
			{
				Paint p = new Paint();
				float x = (float)(canvas.getWidth() * .3);
				float y = (float)(canvas.getHeight() * .4166);
				p.setColor(Color.parseColor("#000000"));
				p.setStyle(Paint.Style.FILL);
				p.setAlpha(Math.min(opacity, 255));

				if (fullscreen) {
					canvas.drawPaint(p);
				}
				else {
					Rect rect = new Rect(10, (int)(y - 30),
						canvas.getWidth() - 10,
						(int)(y + 30));
					canvas.drawRect(rect, p);
				}
				canvas.drawText(message, x, y, paint);
				opacity = ((animate) ? opacity + 10 : 255);
			}
			public void animate(Canvas canvas)
			{
				draw(canvas);
			}
			public void startAnimation()
			{
				opacity = (animate) ? 100 : 255;
			}
		});
	}

	public static void displayNextTurn(final GfxGamePlayer player)
	{
		if (Config.getConfig().doNotify()) {
			String message = "Go "+player.getName();
			Paint paint = new Paint();
			paint.setColor(player.getColor());
			displayMessage(message, paint,
				Animated.DEF_ANIM_DURATION, false, false);
		}
	}

	public static void displayGameOver(final GfxGamePlayer winner)
	{
		String message = "Winner: "+winner.getName();
		Paint paint = new Paint();
		paint.setColor(winner.getColor());
		displayMessage(message, paint,(4 * Animated.DEF_ANIM_DURATION),
			true, true);
	}
}
