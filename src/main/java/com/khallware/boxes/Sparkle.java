// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sparkle implements Animated
{
	private static final Logger logger =
		LoggerFactory.getLogger(Sparkle.class);
	public static final int SPARKLE_COLOR = Color.WHITE;
	public static final double MAX_DISTANCE = (30);
	public static final double MAX_ANIM_TIME = (1 * 1000);
	public static final double MAX_SPEED = (MAX_DISTANCE * 4 / 2000);
	public static final double DEF_SPEED = (10 / 2000); // pixels / millis

	protected int origX = 0;
	protected int origY = 0;
	protected int duration = 0;
	protected int color = SPARKLE_COLOR;
	protected Date beginAnimation = null;
	protected Paint paint = null;
	protected double rateX = 0;
	protected double rateY = 0;
	protected boolean drawnOnce = false;

	public Sparkle(int origX, int origY)
	{
		this.origX = origX;
		this.origY = origY;
		this.rateX = normalizedRandomRate();
		this.rateY = normalizedRandomRate();
		beginAnimation = new Date();
		duration = (int)(Math.random() * MAX_ANIM_TIME);
		paint = new Paint();
	}

	public Sparkle(int origX, int origY, double rateX, double rateY)
	{
		this.origX = origX;
		this.origY = origY;
		this.rateX = rateX;
		this.rateY = rateY;
		beginAnimation = new Date();
		duration = (int)(Math.random() * MAX_ANIM_TIME);
		paint = new Paint();
	}

	private static double normalizedRandomRate()
	{
		double retval = (Math.random() * MAX_SPEED);
		return(retval);
	}

	private static double normalizedRandomRateKDH()
	{
		double retval = 0.0;
		double random = Math.random();
		boolean slow = (Math.random() < 0.5);
		boolean reversed = (Math.random() < 0.5);

		if (random <= 0.25) {  // very slow or very fast
			double min = (slow) ? 0 : (MAX_SPEED - (25 / 2000));
			double max = (slow) ? (5 / 2000) : MAX_SPEED;
			retval = min + (Math.random() * (max - min) + 1);
		}
		else if (random <= 0.50) { // medium slow or medium fast
			double min = (slow) ? (5 / 2000) : (15 / 2000);
			double max = (slow) ? (5 / 2000) : (25 / 1000);
			retval = min + (Math.random() * (max - min) + 1);
		}
		else {  // normal range
			double min = (5 / 2000);
			double max = (15 / 2000);
			retval = min + (Math.random() * (max - min) + 1);
		}
		if (reversed) {
			retval *= -1;
		}
		retval = (retval / 2);  // will combine x and y components
		return(retval);
	}

	public void setColor(int color)
	{
		this.color = color;
	}

	public void setColor(String color)
	{
		this.color = Color.parseColor(color);
	}

	public int getColor()
	{
		return(color);
	}

	public int[] currentPosition()
	{
		int[] retval = new int[] { 0, 0 };
		long time = (new Date().getTime() - beginAnimation.getTime());
		retval[0] = (int)(origX + (rateX * time));
		retval[1] = (int)(origY + (rateY * time));
		return(retval);
	}

	public void draw(Canvas canvas)
	{
		if (Config.getConfig().doAnimTrail()) {
			int[] coord = currentPosition();
			paint.setColor(getColor());
			// canvas.drawPoint(coord[0], coord[1], paint);
			canvas.drawCircle(coord[0], coord[1], 2, paint);
			drawnOnce = true;
		}
	}

	public void startAnimation()
	{
	}

	public void animate(Canvas canvas)
	{
		draw(canvas);
	}

	public boolean isReadyToAnimate()
	{
		return(!isExpired());
	}

	public boolean isExpired()
	{
		boolean retval = false;
		Date expiration = new Date(beginAnimation.getTime() + duration);
		Date now = new Date();
		retval = expiration.before(now);

		if (retval && !drawnOnce) {
			logger.warn("sparkle never drawn to canvas before it "
				+"expired");
		}
		retval = (expiration.before(now));
		retval |= (distanceTraveled() > MAX_DISTANCE);
		return(retval);
	}

	public int distanceTraveled()
	{
		int retval = 0;
		int[] coord = currentPosition();
		retval = (int)Math.sqrt(Math.pow((coord[0] - origX), 2)
			+ Math.pow((coord[1] - origY), 2));
		return(retval);
	}
}
