// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.theme.Theme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.List;

public class GfxGameSquare extends GameSquare implements Graphical
{
	private static final Logger logger =
		LoggerFactory.getLogger(GfxGameSquare.class);

	public GfxGameSquare(List<Edge> edges)
	{
		super(edges);
	}

	public void draw(Canvas canvas)
	{
		if (isComplete()) {
			GfxGamePlayer owner = (GfxGamePlayer)getOwner();
			Theme theme = Config.getConfig().getTheme();
			int opacity = theme.getOpacity(Theme.Item.square);
			Paint paint = new Paint();
			paint.setColor(owner.getColor());
			paint.setStyle(Paint.Style.FILL);
			paint.setAlpha(opacity);
			canvas.drawRect(getDrawArea(2), paint);
		}
	}

	public Rect getDrawArea(int edgeClipSize)
	{
		Rect retval = new Rect();
		float startX = Integer.MAX_VALUE;
		float startY = Integer.MAX_VALUE;
		float stopX = -1;
		float stopY = -1;

		for (Point point : getPoints()) {
			startX = Math.min(
				((GfxGamePoint)point).getXCoord(),
				startX);
			startY = Math.min(
				((GfxGamePoint)point).getYCoord(),
				startY);
			stopX = Math.max(
				((GfxGamePoint)point).getXCoord(),
				stopX);
			stopY = Math.max(
				((GfxGamePoint)point).getYCoord(),
				stopY);
		}
		startX += edgeClipSize;
		startY += edgeClipSize;
		stopX -= edgeClipSize;
		stopY -= edgeClipSize;
		retval.set((int)startX, (int)startY, (int)stopX, (int)stopY);
		return(retval);
	}
}
