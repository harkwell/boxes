// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.theme.*;
import com.khallware.boxes.theme.Theme.Item;
import com.khallware.boxes.chooser.OptimalChooser;
import com.khallware.boxes.chooser.ModerateChooser;
import com.khallware.boxes.chooser.SimpleChooser;
import com.khallware.boxes.chooser.Chooser;
import android.content.res.Resources;
import android.graphics.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config
{
	private static final Logger logger = LoggerFactory.getLogger(
		Config.class);
	private static Config instance = null;
	private static int idx = 0;
	private static List<Theme> themes = new ArrayList<>();
	public enum GameMode { vsEasyCPU, vsMediumCPU, vsHardCPU, vsOther,
		vsOther2 };

	private Map<GfxGamePlayer, Chooser> playerMap = new HashMap<>();
	private GfxGameBoard board = null;
	private int boardWidth = Constants.DEF_BOARD_WIDTH;
	private int boardHeight = Constants.DEF_BOARD_HEIGHT;
	private boolean doVibrate = Constants.DEF_DO_VIBRATE;
	private boolean doDrawTrail = Constants.DEF_DRAW_TRAIL;
	private boolean doAnimTrail = Constants.DEF_ANIM_TRAIL;
	private boolean doNotify = Constants.DEF_NOTIFY_NEXT_GO;
	private GameMode mode = GameMode.vsHardCPU;

	private Config()
	{
		setFactoryDefaults();
	}

	public static synchronized Config getConfig()
	{
		if (instance == null) {
			instance = new Config();
		}
		return(instance);
	}

	public void loadThemes(Resources resources)
	{
		if (themes.size() <= 0) {
			idx = 0;
			themes.add(new Android(resources));
			themes.add(new Tux(resources));
			themes.add(new Blue(resources));
			themes.add(new Winter(resources));
			themes.add(new Fire(resources));
			themes.add(new Beach(resources));
			themes.add(new Samuel(resources));
			themes.add(new Cherry(resources));
			themes.add(new Train(resources));
			themes.add(new Patriotic(resources));
			themes.add(new Logo(resources));
		}
	}

	public Theme nextTheme()
	{
		idx = ((idx+1) % themes.size());
		return(getTheme());
	}

	public int getThemeIndex()
	{
		return(idx);
	}

	public void setThemeIndex(int idx)
	{
		this.idx = idx;
	}

	public Theme getTheme()
	{
		return(themes.get(idx));
	}

	public void setFactoryDefaults()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(Color.parseColor("#51748e"));
		playerMap.put(player, new GfxChooser());

		player = new CPUPlayer(2, "Computer Player");
		player.setColor(Color.parseColor("#e8992c"));
		playerMap.put(player, new OptimalChooser());
	}

	public void setAnimTrail(boolean enabled)
	{
		doAnimTrail = enabled;
	}

	public boolean doAnimTrail()
	{
		return(doAnimTrail);
	}

	public void setDrawTrail(boolean enabled)
	{
		doDrawTrail = enabled;
	}

	public boolean doDrawTrail()
	{
		return(doDrawTrail);
	}

	public void setVibrate(boolean enabled)
	{
		doVibrate = enabled;
	}

	public boolean doVibrate()
	{
		return(doVibrate);
	}

	public void setNotify(boolean enabled)
	{
		doNotify = enabled;
	}

	public boolean doNotify()
	{
		return(doNotify);
	}

	public void setModeToEasy()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(getTheme().getColor(Item.player1));
		playerMap.put(player, new GfxChooser());

		player = new CPUPlayer(2, "Computer Player");
		player.setColor(getTheme().getColor(Item.CPU));
		playerMap.put(player, new SimpleChooser());
		mode = GameMode.vsEasyCPU;
	}

	public void setModeToModerate()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(getTheme().getColor(Item.player1));
		playerMap.put(player, new GfxChooser());

		player = new CPUPlayer(2, "Computer Player");
		player.setColor(getTheme().getColor(Item.player2));
		playerMap.put(player, new ModerateChooser());
		mode = GameMode.vsMediumCPU;
	}

	public void setModeToHard()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(getTheme().getColor(Item.player1));
		playerMap.put(player, new GfxChooser());

		player = new CPUPlayer(2, "Computer Player");
		player.setColor(getTheme().getColor(Item.CPU));
		playerMap.put(player, new OptimalChooser());
		mode = GameMode.vsHardCPU;
	}

	public void setModeToTwoPlayer()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(getTheme().getColor(Item.player1));
		playerMap.put(player, new GfxChooser());

		player = new GfxGamePlayer(2, "Player Two");
		player.setColor(getTheme().getColor(Item.player2));
		playerMap.put(player, new GfxChooser());
		mode = GameMode.vsOther;
	}

	public void setModeToThreePlayer()
	{
		GfxGamePlayer player = null;
		playerMap.clear();
		player = new GfxGamePlayer(1, "Player One");
		player.setColor(getTheme().getColor(Item.player1));
		playerMap.put(player, new GfxChooser());

		player = new GfxGamePlayer(2, "Player Two");
		player.setColor(getTheme().getColor(Item.player2));
		playerMap.put(player, new GfxChooser());

		player = new GfxGamePlayer(3, "Player Three");
		player.setColor(getTheme().getColor(Item.player3));
		playerMap.put(player, new GfxChooser());
		mode = GameMode.vsOther2;
	}

	public GameMode getMode()
	{
		return(mode);
	}

	public void setMode(GameMode mode)
	{
		switch (mode) {
		case vsEasyCPU:
			setModeToEasy();
			break;
		case vsMediumCPU:
			setModeToModerate();
			break;
		case vsHardCPU:
			setModeToHard();
			break;
		case vsOther:
			setModeToTwoPlayer();
			break;
		case vsOther2:
			setModeToThreePlayer();
			break;
		}
	}

	public Map<GfxGamePlayer, Chooser> getPlayerMap()
	{
		return(playerMap);
	}

	public List<Player> getPlayers()
	{
		List<Player> retval = new ArrayList<>();
		retval.addAll(playerMap.keySet());
		return(retval);
	}

	public GfxGameBoard resetAndGetBoard() throws GameException
	{
		board = new GfxGameBoard(getBoardWidth(), getBoardHeight(),
			getPlayers());
		return(board);
	}

	public GfxGameBoard getBoard()
	{
		if (board == null) {
			try {
				resetAndGetBoard();
			}
			catch (GameException e) {
				logger.error(""+e, e);
			}
		}
		return(board);
	}

	public int getBoardWidth()
	{
		return(boardWidth);
	}

	public void setBoardWidth(int boardWidth)
	{
		this.boardWidth = boardWidth;
	}

	public int getBoardHeight()
	{
		return(boardHeight);
	}

	public void setBoardHeight(int boardHeight)
	{
		this.boardHeight = boardHeight;
	}
}
