// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.os.Vibrator;
import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class GfxSurfaceView extends SurfaceView implements Callback
{
	private static final Logger logger = LoggerFactory.getLogger(
		GfxSurfaceView.class);

	protected ThreadManager threadManager = null;
	protected GfxGameEngine gfxGameEngine = null;
	protected GfxGamePoint sourcePoint = null;
	protected Vibrator vibrator = null;

	/** https://groups.google.com/forum/#!msg/android-developers/\
	 * 1QN1qX_Qey4/bnubP8NDEo0J
	 * The SurfaceView is "double buffered", so one must draw every
	 * pixel before calling unlockCanvasAndPost() every time!
	 */
	public GfxSurfaceView(Context context) throws GameException
	{
		super(context);
		threadManager = ThreadManager.getThreadManager();
		getHolder().addCallback(this);
		gfxGameEngine = new GfxGameEngine(getHolder());
		vibrator = (Vibrator)context.getSystemService(
			Context.VIBRATOR_SERVICE);
		logger.debug("GfxSurfaceView() constructor done.");
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		try {
			super.onTouchEvent(event);
			return(gfxGameEngine.getTouchHandler().onTouchEvent(
				event));
		}
		catch (Exception e) {
			logger.error(""+e, e);
			return(Constants.FINISHED_HANDLING);
		}
	}

	public void surfaceCreated(SurfaceHolder holder)
	{
		logger.debug("surfaceCreated()...");
		gfxGameEngine.initialize();
		gfxGameEngine.getBoard().resize(holder.getSurfaceFrame());
		gfxGameEngine.setVibrationHandlers(vibrator);
		try {
			ThreadManager tmgr = ThreadManager.getThreadManager();
			gfxGameEngine.updateUI();
			tmgr.startGameThread(gfxGameEngine);
			tmgr.startRefreshThread(gfxGameEngine);
		}
		catch (GameException e) {
			logger.error(""+e, e);
		}
		gfxGameEngine.setTouchHandler(
			new TouchHandler(gfxGameEngine.getBoard()));
	}

	public void surfaceChanged(SurfaceHolder holder, int format,
			int width, int height)
	{
		logger.debug("surfaceChanged()...");
		gfxGameEngine.setSurfaceHolder(holder);
		gfxGameEngine.getBoard().resize(holder.getSurfaceFrame());
	}

	public void surfaceDestroyed(SurfaceHolder holder)
	{
		logger.debug("surfaceDestroyed()...");
		ThreadManager.getThreadManager().stopGame();
	}
}
