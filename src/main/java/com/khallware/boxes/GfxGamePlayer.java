// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.graphics.Canvas;
import android.graphics.Color;

public class GfxGamePlayer extends GamePlayer implements Graphical
{
	private int color = Color.parseColor("#07992c");

	public GfxGamePlayer(int id, String name)
	{
		super(id, name);
	}

	public void draw(Canvas canvas) throws GameException
	{
	}

	public void setColor(String color)
	{
		setColor(Color.parseColor(color));
	}

	public void setColor(int color)
	{
		this.color = color;
	}

	public int getColor()
	{
		return(color);
	}
}
