// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.theme.Theme;
import com.khallware.boxes.theme.Theme.Item;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GfxGameEdge extends GameEdge implements Animated
{
	private static final Logger logger =
		LoggerFactory.getLogger(GfxGameEdge.class);

	protected int[] coord1 = new int[] { 0, 0 };
	protected int[] coord2 = new int[] { 0, 0 };
	protected boolean taken = false;
	protected Date beginAnimation = new Date();
	protected long duration = Animated.DEF_ANIM_DURATION;
	protected boolean readyToAnimate = false;

	public GfxGameEdge(Edge edge)
	{
		super(edge.getPoint1(), edge.getPoint2());
		updateCoords();
	}

	public GfxGameEdge(GfxGamePoint point1, GfxGamePoint point2)
	{
		super(point1, point2);
		updateCoords();
	}

	@Override
	public boolean take()
	{
		boolean retval = super.take();
		taken = true;
		return(retval);
	}

	protected void updateCoords()
	{
		coord1[0] = ((GfxGamePoint)point1).getXCoord();
		coord1[1] = ((GfxGamePoint)point1).getYCoord();
		coord2[0] = ((GfxGamePoint)point2).getXCoord();
		coord2[1] = ((GfxGamePoint)point2).getYCoord();
	}

	public int[] getCoord1()
	{
		updateCoords();
		return(coord1);
	}

	public int[] getCoord2()
	{
		updateCoords();
		return(coord2);
	}

	public void draw(Canvas canvas)
	{
		draw(canvas, taken);
	}

	protected void draw(Canvas canvas, boolean taken)
	{
		Theme theme = Config.getConfig().getTheme();
		float startX = getCoord1()[0];
		float startY = getCoord1()[1];
		float stopX = getCoord2()[0];
		float stopY = getCoord2()[1];
		Paint paint = new Paint();
		int takenColor = theme.getColor(Item.takenEdge);
		int untakenColor = theme.getColor(Item.untakenEdge);
		paint.setStyle(Paint.Style.FILL);

		if (taken) {
			startX -=1;
			stopX +=1;
			startY -= 1;
			stopY += 1;
			paint.setColor(takenColor);
			paint.setAlpha(theme.getOpacity(Item.takenEdge));
			canvas.drawRect(startX, startY, stopX, stopY, paint);
		}
		else {
			paint.setColor(untakenColor);
			paint.setAlpha(theme.getOpacity(Item.untakenEdge));
			canvas.drawLine(startX, startY, stopX, stopY, paint);
		}
	}

	public void startAnimation()
	{
		beginAnimation = new Date();
		readyToAnimate = true;
	}

	public void animate(Canvas canvas)
	{
		draw(canvas, true);
	}

	public boolean isReadyToAnimate()
	{
		return(readyToAnimate);
	}

	public boolean isExpired()
	{
		boolean retval = true;
		Date expiration = new Date(beginAnimation.getTime() + duration);
		Date now = new Date();
		retval = expiration.before(now);

		if (retval) {
			readyToAnimate = false;
		}
		return(retval);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(super.toString()+" ")
			.toString());
	}
}
