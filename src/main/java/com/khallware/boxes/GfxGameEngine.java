// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.os.Vibrator;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import com.khallware.boxes.chooser.Util;
import com.khallware.boxes.chooser.Chooser;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GfxGameEngine extends GameEngine
{
	private static final Logger logger =
		LoggerFactory.getLogger(GfxGameEngine.class);

	private Datastore dstore = Datastore.getDatastore();
	private SurfaceHolder surfaceHolder = null;
	private TouchHandler touchHandler = null;
	private Config cfg = Config.getConfig();
	private GfxGameBoard board = null;
	private Vibrator vibrator = null;

	public GfxGameEngine(SurfaceHolder surfaceHolder) throws GameException
	{
		this.surfaceHolder = surfaceHolder;
		board = cfg.resetAndGetBoard();
		logger.info("GfxGameEngine constructed");
	}

	@Override
	public void beforeFirstMove()
	{
		int id = Datastore.getDatastore().playersTurn();
		super.beforeFirstMove();
		saveGamePlayers();
		moves = players.indexOf(new GamePlayer(id, "foo"));
		moves = (moves == Datastore.UNKNOWN) ? 0 : moves;
		logger.debug("setting moves value to "+moves);
	}

	@Override
	public void beforeMove(Player p1)
	{
		boolean flag = true;
		GfxGamePlayer player = (GfxGamePlayer)p1;
		super.beforeMove(player);
		dstore.saveState(player);

		for (Player p2 : players) {
			flag &= (!(p2 instanceof CPUPlayer));
		}
		if (flag) {
			GfxUtil.displayNextTurn(player);
		}
	}

	@Override
	public void afterMove(Player player, Edge edge)
	{
		if (player instanceof CPUPlayer) {
			animateMove((GfxGameEdge)edge);
		}
		super.afterMove(player, edge);
		try {
			dstore.saveMove(player.getId(),
				edge.getPoint1().getId(),
				edge.getPoint2().getId());
		}
		catch (DatastoreException e) {
			logger.error(""+e, e);
			done = true;
		}
	}

	@Override
	public void afterLastMove()
	{
		super.afterLastMove();
		try {
			dstore.prepForNewGame();
			dstore.saveState(players.get(0));
			cfg.nextTheme();
			cfg.setMode(cfg.getMode());
			dstore.saveConfig();
			cleanup();
			initialize();
			getBoard().resize(surfaceHolder.getSurfaceFrame());
			setVibrationHandlers(vibrator);
			setTouchHandler(new TouchHandler(getBoard()));
		}
		catch (DatastoreException e) {
			logger.error(""+e, e);
		}
	}

	public void setSurfaceHolder(SurfaceHolder surfaceHolder)
	{
		logger.debug("setSurfaceHolder()...");
		this.surfaceHolder = surfaceHolder;
	}

	public TouchHandler getTouchHandler()
	{
		return(touchHandler);
	}

	public void setTouchHandler(TouchHandler touchHandler)
	{
		this.touchHandler = touchHandler;
	}

	public void initialize()
	{
		boolean flag = false;
		try {
			board = Datastore.getDatastore().loadGameBoard();
			flag = true;

			if (board.getPoints().size() == 0) {
				flag = false;
			}
		}
		catch (DatastoreException e) {
			logger.error(""+e, e);
		}
		finally {
			if (!flag) {
				try {
					board = cfg.resetAndGetBoard();
				}
				catch (GameException e) {
					logger.error(""+e, e);
				}
			}
		}
	}

	public void cleanup()
	{
		getBoard().getEdges().clear();
		getBoard().getSquares().clear();
		getBoard().newlyCompletedSquares().clear();
	}

	public GfxGamePlayer playGame() throws GameException
	{
		return((GfxGamePlayer)playGame(cfg.getPlayerMap(), getBoard()));
	}

	public void playLoop()
	{
		GfxGamePlayer winner = null;

		while (true) {
			try {
				winner = playGame();
				ThreadManager.getThreadManager().clearQueue();
				GfxUtil.displayGameOver(winner);
				logger.info("player ("+winner+") won the game");
			}
			catch (GameException e) {
				GfxUtil.displayMessage(""+e, new Paint(),
					(10*1000), true, false);
			}
		}
	}

	public void updateUI() throws GameException
	{
		Canvas canvas = null;

		for (Animated anim : GfxUtil.findAnimated(getBoard())) {
			if (anim.isReadyToAnimate()) {
				ThreadManager.getThreadManager().enqueue(
					(Graphical)anim);
			}
		}
		try {
			synchronized (surfaceHolder) {
				canvas = surfaceHolder.lockCanvas();

				if (canvas != null) {
					draw(canvas);
				}
				else {
					logger.error("unable to lock canvas!");
				}
			}
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
		finally {
			if (canvas != null) {
				surfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}

	public void setVibrationHandlers(final Vibrator vibrator)
	{
		this.vibrator = vibrator;

		for (Point point : getBoard().getPoints()) {
			((GfxGamePoint)point).addSelectHandler(
					new GfxGamePoint.SelectHandler() {
				public void onSelection(boolean selected)
				{
					if (cfg.doVibrate()) {
						vibrator.vibrate(15);
					}
				}
			});
		}
	}

	public void draw(Canvas canvas) throws GameException
	{
		int count = 0;
		Graphical graphical = getBoard();
		List<Graphical> list = new ArrayList<>();
		ThreadManager mgr = ThreadManager.getThreadManager();
		graphical.draw(canvas);

		while ((graphical = mgr.dequeue()) != null) {
			graphical.draw(canvas);
			count++;

			if (graphical instanceof Animated) {
				if (!((Animated)graphical).isExpired()) {
					list.add(graphical);
				}
			}
			if (count > Constants.MAX_NUM_DRAWITEMS) {
				logger.error("maximum number of graphical items "
					+"have been drawn, ignoring the rest!");
				mgr.clearQueue();
				break;
			}
		}
		for (Graphical g : list) {
			mgr.enqueue(g);
		}
	}

	public GfxGameBoard getBoard()
	{
		return(board);
	}

	protected void animateMove(GfxGameEdge edge)
	{
		int[] coord1 = edge.getCoord1();
		int[] coord2 = edge.getCoord2();

		if (coord1[1] == coord2[1]) { // horizontal
			int x = Math.min(coord2[0], coord1[0]);

			for (int i=0; i<Math.abs(coord2[0]-coord1[0]); i++) {
				for (int j=0; j<Constants.SPARKLE_RATE; j++) {
					ThreadManager.getThreadManager()
						.enqueue(new Sparkle(x,
							coord1[1]));
				}
				x++;
			}
		}
		else { //vertical
			int y = Math.min(coord2[1], coord1[1]);

			for (int i=0; i<Math.abs(coord2[1]-coord1[1]); i++) {
				for (int j=0; j<Constants.SPARKLE_RATE; j++) {
					ThreadManager.getThreadManager()
						.enqueue(new Sparkle(coord1[0],
							y));
				}
				y++;
			}
		}
	}

	protected void saveGamePlayers()
	{
		logger.debug("saveGamePlayers()...");
		try {
			dstore.truncatePlayers();

			for (Object p : players) {
				GfxGamePlayer player = (GfxGamePlayer)p;
				dstore.setPlayer(player, map.get(player));
			}
		}
		catch (DatastoreException e) {
			logger.error(""+e, e);
		}
	}
}
