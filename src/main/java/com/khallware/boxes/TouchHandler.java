// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.chooser.Util;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class TouchHandler
{
	private static final Logger logger = LoggerFactory.getLogger(
		TouchHandler.class);
	public static final int THRESHOLD = Constants.TOUCH_THRESHOLD;

	protected static MotionEvent lastEvent = null;

	protected GfxGamePoint sourcePoint = null;
	protected GfxGameBoard board = null;
	protected Paint paint = new Paint();
	protected boolean running = false;
	protected boolean doDrawTrail = Constants.DEF_DRAW_TRAIL;

	public TouchHandler(GfxGameBoard board)
	{
		this.board = board;
	}

	public boolean onTouchEvent(MotionEvent event)
	{
		boolean retval = false;
		try {
			retval = onTouchEventUnwrapped(event);
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
		return(retval);
	}

	public void setSourcePoint(MotionEvent event)
	{
		setSourcePoint(event, false);
	}

	public void setSourcePoint(MotionEvent event, boolean replace)
	{
		int x = (int)event.getX();
		int y = (int)event.getY();
		logger.debug("onTouchEvent(): x="+x+" y="+y);

		if (replace || getSourcePoint() == null) {
			if (getSourcePoint() != null) {
				getSourcePoint().select(false);
			}
			setSourcePoint(locatePoint(x, y));
		}
	}

	public void setSourcePoint(GfxGamePoint sourcePoint)
	{
		boolean callSelect = (sourcePoint != null);
		boolean callDeselect = (getSourcePoint() != null
			&& (sourcePoint == null
			|| !getSourcePoint().equals(sourcePoint)));

		if (callDeselect) {
			getSourcePoint().select(false);
		}
		if (callSelect) {
			sourcePoint.select(true);
		}
		this.sourcePoint = sourcePoint;
	}

	public GfxGamePoint getSourcePoint()
	{
		return(sourcePoint);
	}

	/*
	 * It is possible that MotionEvent.ACTION_UP actions become missing.
	 * The lastEvent attribute can help determine whether we should take
	 * an edge or not.  The current event will not carry the event history
	 * if it's ACTION_UP.
	 */
	protected synchronized boolean onTouchEventUnwrapped(MotionEvent event)
	{
		boolean retval = !Constants.FINISHED_HANDLING;
		GfxGamePoint destPoint = null;
		int x = (int)event.getX();
		int y = (int)event.getY();
		setSourcePoint(event, (lastEvent == null)); // on first touch
		destPoint = locatePoint(x, y);
		retval = (getSourcePoint() != null); // continue touch events?

		if (retval) {
			drawMotionEventHistory(event, true);
		}
		switch (event.getAction()) {
		case MotionEvent.ACTION_MOVE:
			lastEvent = event;
			drawMotionEventHistory(lastEvent, false);

			if (Config.getConfig().doAnimTrail()) {
				for (int i=0; i<Constants.SPARKLE_RATE; i++) {
					ThreadManager.getThreadManager()
						.enqueue(new Sparkle(
							(int)event.getX(),
							(int)event.getY()));
				}
				// logger.debug("sparkle...");
			}
			break;
		case MotionEvent.ACTION_UP:
			retval = Constants.FINISHED_HANDLING; // end processing
			logger.debug("UP x="+x+" y="+y);
			lastEvent = (lastEvent != null) ? lastEvent : event;
			drawMotionEventHistory(lastEvent, true);
			// lastEvent.recycle();  WHY CALLED TWICE!?
			lastEvent = null;

			if (sourcePoint == null) {
				logger.error("source point is null");
			}
			else if (destPoint == null) {
				if (sourcePoint != null) {
					sourcePoint.select(false);
					sourcePoint = null;
				}
				logger.error("dest point is null");
			}
			else if (destPoint == sourcePoint) {
				if (sourcePoint != null) {
					sourcePoint.select(false);
					sourcePoint = null;
				}
				logger.debug("selected same point");
				setSourcePoint((GfxGamePoint)null);
			}
			else {
				logger.info("two points selected");
				destPoint.select(true);
			}
			break;
		}
		return(retval);
	}

	protected Paint getPaint()
	{
		return(paint);
	}

	protected void drawMotionEventHistory(final MotionEvent event,
			final boolean erase)
	{
		ThreadManager.getThreadManager().enqueue(new Graphical() {
			public void draw(Canvas canvas) throws GameException
			{
				int len = event.getHistorySize();

				for (int idx=0; idx < len; idx++) {
					int x = (int)event.getHistoricalX(idx);
					int y = (int)event.getHistoricalY(idx);

					if (!Config.getConfig().doDrawTrail()) {
						break;
					}
					getPaint().setColor(Color.parseColor(
						(erase)
							? "#000000"
							: "#e5e347")
					);
					canvas.drawCircle(x, y, 3, getPaint());
				}
			}
		});
	}

	protected int[] getReleasePoint(MotionEvent event)
	{
		int[] retval = new int[] { -1,-1 };
		int idx = -1;

		if (event.getHistorySize() > 0) {
			if ((idx = (event.getHistorySize() - 1)) >= 0) {
				retval[0] = (int)event.getHistoricalX(idx);
				retval[1] = (int)event.getHistoricalY(idx);
			}
		}
		return(retval);
	}

	protected GfxGamePoint locatePoint(int x, int y)
	{
		GfxGamePoint retval = null;
		//Rect rect = getMaxTouchRect(x, y);
		Rect rect = getTouchRect(x, y, 20);

		for (Point point : board.getPoints()) {
			GfxGamePoint p = (GfxGamePoint)point;

			if (rect.contains(p.getXCoord(), p.getYCoord())) {
				retval = p;
				break;
			}
		}
		return(retval);
	}

	protected Rect getMaxTouchRect(int x, int y)
	{
		return(getTouchRect(x, y, Constants.MAX_TOUCH_THRESHOLD));
	}

	protected Rect getTouchRect(int x, int y)
	{
		return(getTouchRect(x, y, THRESHOLD));
	}

	protected Rect getTouchRect(int x, int y, int  threshold)
	{
		Rect retval = new Rect();
		int left = (x - threshold);
		int top = (y - threshold);
		int right = (x + threshold);
		int bottom = (y + threshold);

		if (threshold == Constants.MAX_TOUCH_THRESHOLD) {
			GfxGameSquare square = (GfxGameSquare)
				Util.getFirstSquare(board);
			retval = square.getDrawArea(1);
			retval.offsetTo((x - retval.centerX()),
				(y - retval.centerY()));
		}
		retval.set(left, top, right, bottom);
		return(retval);
	}
}
