// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

public class Constants
{
	public static final String PROPHEAD = "boxes.";
	public static final String DATE_FORMAT = "yyyyMMdd";
	//public static final int FRAME_DELAY = (int)((1/60) * 1000);  // 60 fps
	public static final int FRAME_DELAY = (int)((1/4) * 1000);  // 4 fps
	public static final int MAX_NUM_DRAWITEMS = 500;
	public static final int MAXTHREADS = 800;
	public static final int TOUCH_THRESHOLD = 15;
	public static final int MAX_TOUCH_THRESHOLD = -1;
	public static final int DEF_BOARD_WIDTH = 5;
	public static final int DEF_BOARD_HEIGHT = 5;
	public static final int DEF_OPACITY = 255;
	public static final int SPARKLE_RATE = 5;
	public static final boolean DEF_ANIM_TRAIL = true;
	public static final boolean DEF_DO_VIBRATE = true;
	public static final boolean DEF_DRAW_TRAIL = false;
	public static final boolean DEF_NOTIFY_NEXT_GO = true;
	public static final boolean FINISHED_HANDLING = true;
}
