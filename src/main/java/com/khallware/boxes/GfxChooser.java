// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.chooser.Util;
import com.khallware.boxes.chooser.Chooser;
import com.khallware.boxes.chooser.RandomChooser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GfxChooser implements Chooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		GfxChooser.class);
	public static final long POINT_SELECTION_DELAY = (long)(.25 * 1000);

	public GfxChooser() { }

	public Edge choose(Board board)
	{
		Edge retval = blockOnEdgeSelection((GfxGameBoard)board);
		logger.info("user selected edge ("+retval+")");
		return(retval);
	}

	protected Edge blockOnEdgeSelection(GfxGameBoard board)
	{
		Edge retval = null;
		boolean flag = false;
		logger.debug("blockOnEdgeSelection()...");

		while ((retval = findSelectedEdge(board)) == null) {
			if (flag) {
				logger.debug("no edge selected yet...");
				flag = true;
			}
			try {
				Thread.sleep(POINT_SELECTION_DELAY);
			}
			catch (InterruptedException e) {
				logger.error(""+e, e);
				break;
			}
		}
		logger.debug("edge selected ("+retval+")");
		((Animated)retval).startAnimation();
		return(retval);
	}

	protected Edge findSelectedEdge(GfxGameBoard board)
	{
		Edge retval = null;
		Point pt1 = null;
		Point pt2 = null;

		for (Point point : board.getPoints()) {
			if (!((GfxGamePoint)point).isSelected()) {
				continue;
			}
			else if (pt1 == null) {
				pt1 = point;
			}
			else if (pt2 == null) {
				pt2 = point;
				logger.debug("found two selected points: "
					+"("+pt1+") and ("+pt2+")");
				((GfxGamePoint)pt1).select(false);
				((GfxGamePoint)pt2).select(false);
				retval = Util.findEdge(new GameEdge(pt1, pt2),
					board.getSquares());
				break;
			}
		}
		return(retval);
	}
}
