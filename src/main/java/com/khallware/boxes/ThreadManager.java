// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ThreadManager
{
	private static final Logger logger = LoggerFactory.getLogger(
		ThreadManager.class);
	public static final int FRAME_DELAY = Constants.FRAME_DELAY;

	protected static ExecutorService svc = null;
	protected static ConcurrentLinkedQueue<Graphical> queue = null;
	protected static ThreadManager instance = null;
	protected boolean gameRunning = false;

	private ThreadManager()
	{
		queue = new ConcurrentLinkedQueue<>();
		svc = Executors.newFixedThreadPool(Constants.MAXTHREADS);
		logger.debug("ThreadManager() singleton constructor called...");
	}

	public static synchronized final ThreadManager getThreadManager()
	{
		if (instance == null) {
			instance = new ThreadManager();
		}
		return(instance);
	}

	public void clearQueue()
	{
		queue.clear();
	}

	public void enqueue(Graphical graphical)
	{
		queue.add(graphical);
	}

	public Graphical dequeue()
	{
		return(queue.poll());
	}

	public void stopGame()
	{
		gameRunning = false;
		svc.shutdownNow();
		svc = Executors.newFixedThreadPool(Constants.MAXTHREADS);
	}

	public void startGameThread(final GfxGameEngine gfxGameEngine)
	{
		try {
			startGameThreadUnwrapped(gfxGameEngine);
		}
		catch (Exception e) {
			logger.error(""+e, e);
		}
	}
	
	protected void startRefreshThread(final GfxGameEngine gfxGameEngine)
	{
		svc.submit(new Runnable() {
		public void run()
		{
			long prev = System.currentTimeMillis();
			long curr = -1;
			long diff = -1;

			while (gameRunning) {
				curr = System.currentTimeMillis();

				if ((diff = (curr - prev)) < FRAME_DELAY) {
					try {
						Thread.sleep(FRAME_DELAY-diff);
					}
					catch (InterruptedException e) {
						break;
					}
				}
				prev = curr;
				try {
					gfxGameEngine.updateUI();
				}
				catch (GameException e) {
					logger.error(""+e, e);
				}
			}
		}});
	}

	private void startGameThreadUnwrapped(final GfxGameEngine engine)
	{
		gameRunning = true;
		svc.submit(new Runnable() {
			public void run()
			{
				logger.info("game loop thread starting...");
				engine.playLoop();
				logger.info("game loop thread ending...");
			}
		});
	}
}
