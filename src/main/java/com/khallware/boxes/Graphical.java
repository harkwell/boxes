// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import android.graphics.Canvas;

public interface Graphical
{
	public void draw(Canvas canvas) throws GameException;
}
