// copyright Kevin D.Hall 2013-2017

package com.khallware.boxes;

import com.khallware.boxes.chooser.FirstAvailableChooser;
import com.khallware.boxes.chooser.OptimalChooser;
import com.khallware.boxes.chooser.RandomChooser;
import com.khallware.boxes.chooser.Chooser;
import com.khallware.boxes.chooser.Util;
import java.util.Properties;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class GameTest
{
	private static final Logger logger =
		LoggerFactory.getLogger(GameTest.class);

	private List<Player> players = null;

	@Before
	public void GameTest() throws Exception
	{
		players = new ArrayList<Player>();
		players.add(new GamePlayer(1, "player one"));
		players.add(new GamePlayer(2, "player two"));
		players.add(new GamePlayer(3, "player three"));
	}

	@Test
	public void tooFewPlayersTest() throws Exception
	{
		boolean flag = false;
		List<Player> list = new ArrayList<Player>();
		GameBoard.enforceNumPlayers = true;
		logger.info("tooFewPlayers()...");
		list.add(players.get(0));
		try {
			Board board = new GameBoard(10, 10, list);
		}
		catch (GameException e) {
			logger.error(""+e, e);
			flag = true;
		}
		assertTrue(flag);
	}

	@Test
	public void shortGameTest() throws Exception
	{
		logger.info("shortGameTest()...");
		Board board = new GameBoard(1, 1, players);
		Player player = players.get(0);
		Point p1 = board.getPoint(0);
		Point p2 = board.getPoint(1);
		logger.info(""+board);
		logger.info("move "+p1+", "+p2);
		board.move(player, new GameEdge(p1, p2));
		Util.increaseScore(board, player);
		logger.info(""+board);

		player = players.get(1);
		p1 = board.getPoint(1);
		p2 = board.getPoint(3);
		board.move(player, new GameEdge(p1, p2));
		Util.increaseScore(board, player);
		logger.info(""+board);

		player = players.get(0);
		p1 = board.getPoint(2);
		p2 = board.getPoint(3);
		board.move(player, new GameEdge(p1, p2));
		Util.increaseScore(board, player);
		logger.info(""+board);

		player = players.get(1);
		p1 = board.getPoint(0);
		p2 = board.getPoint(2);
		board.move(player, new GameEdge(p1, p2));
		Util.increaseScore(board, player);
		logger.info(""+player);
		logger.info(""+board);
		assertTrue(player.getSquares().size() == 1);
	}

	@Test
	public void normalGameTest() throws Exception
	{
		logger.info("normalGameTest()...");
		Board board = new GameBoard(10, 10, players);
		Player player = players.get(0);
		Chooser chooser = new FirstAvailableChooser();
		Edge edge = null;
		int moves = 0;

		for (boolean done = false; !done;) {
			boolean myTurn = false;
			player = players.get((moves % players.size()));
			do {
				if ((edge = chooser.choose(board)) == null) {
					done = true;
					logger.info("game over with "+moves
						+" moves");
					break;
				}
				done = (!board.move(player, edge));
				myTurn = Util.increaseScore(board, player);
				logger.info(""+board);
				moves++;
			}
			while (myTurn);
		}
		for (Player p : players) {
			if (player == null || p.getSquares().size()
					> player.getSquares().size()) {
				player = p;
			}
		}
		logger.info(""+player+" wins");
		assertTrue(player != null);
	}

	@Test
	public void strategicGameTest() throws Exception
	{
		logger.info("strategicGameTest()...");
		Engine engine = new GameEngine();
		Board board = null;
		Map<Player, Chooser> map = new HashMap<Player, Chooser>();
		Player winner = null;
		map.put(players.get(0), new FirstAvailableChooser());
		map.put(players.get(1), new RandomChooser());
		map.put(players.get(2), new OptimalChooser());
		board = new GameBoard(10, 10, players);
		winner = engine.playGame(map, board);
		logger.info(""+winner+" wins");
		assertTrue(winner != null);
	}
}
