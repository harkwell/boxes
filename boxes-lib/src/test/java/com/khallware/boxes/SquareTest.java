// copyright Kevin D.Hall 2013-2017

package com.khallware.boxes;

import com.khallware.boxes.chooser.Util;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class SquareTest
{
	private static final Logger logger =
		LoggerFactory.getLogger(SquareTest.class);

	protected Square square = null;
	protected Point p1 = new GamePoint(0, 4);
	protected Point p2 = new GamePoint(1, 4);
	protected Point p3 = new GamePoint(2, 4);
	protected Point p4 = new GamePoint(3, 4);

	@Before
	public void SquareTest() throws Exception
	{
		square = new GameSquare(Util.createEdges(p1, p2, p3, p4));
	}

	@Test
	public void numberOfEdgesTest() throws Exception
	{
		int num = square.getEdges().size();
		logger.info("numberOfEdgesTest()...");
		logger.debug("number of edges: "+num);
		assertTrue(num == 4);
	}

	@Test
	public void numberOfPointsTest() throws Exception
	{
		int num = square.getPoints().size();
		logger.info("numberOfPointsTest()...");
		logger.debug("number of points: "+num);
		assertTrue(num == 4);
	}

	@Test
	public void completionTest() throws Exception
	{
		Edge edge = null;
		logger.info("completionTest()...");
		assertTrue(!square.isComplete());

		List<Square> list = new ArrayList<Square>();
		list.add(square);
		edge = Util.findEdge(new GameEdge(p1, p2), list);
		edge.take();
		logger.debug(""+square);
		assertTrue(square.getTakenEdges().size() == 1);

		edge = Util.findEdge(new GameEdge(p2, p4), list);
		edge.take();
		logger.debug(""+square);
		assertTrue(square.getTakenEdges().size() == 2);

		edge = Util.findEdge(new GameEdge(p3, p4), list);
		edge.take();
		logger.debug(""+square);
		assertTrue(square.getTakenEdges().size() == 3);

		edge = Util.findEdge(new GameEdge(p1, p3), list);
		edge.take();
		logger.debug(""+square);
		assertTrue(square.getTakenEdges().size() == 4);
		assertTrue(p1.numReferences() == 2);
		assertTrue(p2.numReferences() == 2);
		assertTrue(p3.numReferences() == 2);
		assertTrue(p4.numReferences() == 2);
		assertTrue(square.isComplete());
	}
}
