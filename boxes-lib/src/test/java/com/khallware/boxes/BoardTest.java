// copyright Kevin D.Hall 2013-2017

package com.khallware.boxes;

import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;

public class BoardTest
{
	private static final Logger logger =
		LoggerFactory.getLogger(BoardTest.class);

	@Before
	public void BoardTest() throws Exception
	{
		GameBoard.enforceNumPlayers = false;
	}

	@Test
	public void numberOfEdgesTest() throws Exception
	{
		Board board = new GameBoard(1, 1, null);
		int num = board.getEdges().size();
		logger.info("numberOfEdgesTest()...");
		logger.debug("number of edges: "+num);
		assertTrue(num == 4);

		board = new GameBoard(10, 10, null);
		num = board.getEdges().size();
		logger.debug("number of edges: "+num);
		assertTrue(num == 400);
	}

	@Test
	public void numberOfSquaresTest() throws Exception
	{
		Board board = new GameBoard(1, 1, null);
		int num = board.getSquares().size();
		logger.info("numberOfSquaresTest()...");
		logger.debug("number of squares: "+num);
		assertTrue(num == 1);

		board = new GameBoard(10, 10, null);
		num = board.getSquares().size();
		logger.debug("number of squares: "+num);
		assertTrue(num == 100);
	}
}
