// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

public interface Edge<P extends Point>
{
	public boolean take();
	public P getPoint1();
	public P getPoint2();
	public boolean hasHandlers();
	public void register(EdgeHandler handler);
}
