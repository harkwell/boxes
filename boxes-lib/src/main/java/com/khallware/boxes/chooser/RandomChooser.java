// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Board;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RandomChooser implements Chooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		RandomChooser.class);

	public RandomChooser() { }

	public Edge choose(Board board)
	{
		Edge retval = null;
		List<Edge> list = new ArrayList<>();
		int idx = 0;

		for (Object obj : board.getEdges()) {
			Edge edge = (Edge)obj;

			if (!Util.isTaken(board, edge)) {
				list.add(edge);
			}
		}
		idx = Math.max((int)(Math.random() * list.size()), 1);
		logger.debug("random idx=\""+retval+"\"");
		retval = list.get(idx-1);
		logger.debug("next random edge: ("+retval+")");
		return(retval);
	}
}
