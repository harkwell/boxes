// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Point;
import com.khallware.boxes.Square;
import com.khallware.boxes.GameEdge;
import com.khallware.boxes.GameBoard;
import com.khallware.boxes.Player;
import com.khallware.boxes.Board;
import com.khallware.boxes.Edge;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util
{
	public static final Logger logger = LoggerFactory.getLogger(Util.class);

	public static List<Point> allPoints(Board board)
	{
		List<Point> retval = new ArrayList<>();
		Point[][] grid = board.getPointGrid();

		for (int row=0; row < grid.length; row++) {
			for (int col=0; col < grid[row].length; col++) {
				retval.add(grid[row][col]);
			}
		}
		return(retval);
	}

	public static List<Integer[]> adjacentIndices(Point point, Board board)
	{
		List<Integer[]> retval = new ArrayList<>();
		Point[][] grid = board.getPointGrid();
		int[] coord = findInGrid(point, board);
		int row = coord[0];
		int col = coord[1];
		Point p  = null;

		if (col > 0) {
			retval.add(new Integer[] { row, (col - 1) });
		}
		if (col < grid[0].length) {
			retval.add(new Integer[] { row, (col + 1) });
		}
		if (row > 0) {
			retval.add(new Integer[] { (row - 1), col });
		}
		if (row < grid.length) {
			retval.add(new Integer[] { (row + 1), col });
		}
		return(retval);
	}

	public static Edge findEdge(Edge edge, List<Square> squares)
	{
		Edge retval = null;
		boolean match = false;

		LOOP: for (Square square : squares) {
			for (Object obj : square.getEdges()) {
				Edge e = (Edge)obj;
				match = e.getPoint1().equals(edge.getPoint1());
				match &= e.getPoint2().equals(edge.getPoint2());

				if (match) {
					retval = e;
					break LOOP;
				}
			}
		}
		return(retval);
	}

	public static int[] findInGrid(Point point, Board board)
	{
		int[] retval = new int[] { -1, -1 };
		Point[][] grid = board.getPointGrid();

		LOOP: for (int row=0; row < grid.length; row++) {
			for (int col=0; col < grid[row].length; col++) {
				if (point.equals(grid[row][col])) {
					retval[0] = row;
					retval[1] = col;
					break LOOP;
				}
			}
		}
		return(retval);
	}

	public static List<Square> squaresHavingEdge(GameBoard board, Edge edge)
	{
		List<Square> retval = new ArrayList<>();
		Edge foundEdge = findEdge(edge, board.getSquares()); 
		logger.debug("board has "+board.getSquares().size()+" squares");

		if (foundEdge != null) {
			for (Square square : board.getSquares()) {
				logger.debug("square "+square.hashCode()+" "
					+"has "+square.getEdges().size()+" "
					+"edges");

				if (square.getEdges().contains(foundEdge)) {
					retval.add(square);
				}
			}
		}
		else {
			logger.error("did not find edge! ("+edge+")");
		}
		return(retval);
	}

	public static boolean isTaken(Board board, Edge edge)
	{
		boolean retval = false;
		Edge foundEdge = findEdge(edge, board.getSquares()); 

		for (Object obj : board.getSquares()) {
			Square square = (Square)obj;
			retval |= square.getTakenEdges().contains(foundEdge);
		}
		return(retval);
	}

	public static boolean isEdge(Board board, Point p1, Point p2)
	{
		boolean retval = (p1 != p2);
		int[] idx1 = Util.findInGrid(p1, board);
		int[] idx2 = Util.findInGrid(p2, board);
		int row1 = idx1[0];
		int row2 = idx2[0];
		int col1 = idx1[1];
		int col2 = idx2[1];
		int val = Math.abs(row1 - row2);
		retval &= (val == 1 || val == 0);
		val = Math.abs(col1 - col2);
		retval &= (val == 1 || val == 0);
		return(retval);
	}

	public static boolean increaseScore(Board board, Player player)
	{
		boolean retval = false;
		List<Square> squares = board.newlyCompletedSquares();
		player.getSquares().addAll(squares);

		for (Square square : squares) {
			square.setOwner(player);
		}
		logger.debug("found "+squares.size()+" new squares");
		retval = (squares.size() > 0);
		return(retval);
	}

	public static List<Edge> createEdges(Point p1, Point p2, Point p3,
			Point p4)
	{
		List<Edge> retval = new ArrayList<>();
		retval.add(new GameEdge(p1, p2));
		retval.add(new GameEdge(p2, p4));
		retval.add(new GameEdge(p3, p4));
		retval.add(new GameEdge(p1, p3));
		return(retval);
	}

	public static List<Edge> createOrFindEdges(Board board, Point p1,
			Point p2, Point p3, Point p4)
	{
		List<Edge> retval = new ArrayList<>();
		Edge tmp = null;

		for (Edge edge : createEdges(p1, p2, p3, p4)) {
			if ((tmp=findEdge(edge, board.getSquares())) == null) {
				retval.add(edge);
			}
			else {
				retval.add(tmp);
			}
		}
		return(retval);
	}

	public static Edge findLastEdgeOfSquare(Square square)
	{
		Edge retval = null;

		for (Object obj : square.getEdges()) {
			Edge edge = (Edge)obj;

			if (square.getTakenEdges().contains(edge)) {
				continue;
			}
			retval = edge;
			break;
		}
		return(retval);
	}

	public static List<Edge> findFinalEdges(Board board)
	{
		List<Edge> retval = new ArrayList<>();
		Edge edge = null;

		for (Object obj : board.getSquares()) {
			Square square = (Square)obj;

			if (square.getTakenEdges().size() != 3) {
				continue;
			}
			edge = findLastEdgeOfSquare(square);

			if (edge != null && !retval.contains(edge)) {
				retval.add(edge);
			}
		}
		return(retval);
	}

	public static boolean isGameOver(GameBoard board)
	{
		boolean retval = true;

		for (Point point : board.getPoints()) {
			retval &= (!point.hasAvailableReference());
		}
		return(retval);
	}

	public static Square getFirstSquare(Board board)
	{
		Square retval = null;

		for (Object obj : board.getSquares()) {
			if (obj != null) {
				retval = (Square)obj;
				break;
			}
		}
		return(retval);
	}
}
