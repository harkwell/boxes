// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Board;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This chooser tries to find first, any final edges and then the safest
 * edge (those with the highest point values).
 */
public class ModerateChooser implements Chooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		ModerateChooser.class);

	public ModerateChooser() { }

	public Edge choose(Board board)
	{
		Edge retval = null;
		List<Edge> finalEdges = Util.findFinalEdges(board);

		if (finalEdges.size() > 0) {
			logger.debug("found "+finalEdges.size()+" final edges");
			retval = finalEdges.get(0);
		}
		else if ((retval = findFirstSafeEdge(board)) == null) {
			retval = findEdgeWithHighestScore(board);
		}
		logger.debug("next optimal edge: ("+retval+")");
		return(retval);
	}

	protected boolean isTrulySafe(Edge edge)
	{
		int refs1 = edge.getPoint1().numReferences();
		int refs2 = edge.getPoint2().numReferences();
		logger.debug("refs1="+refs1+" refs2="+refs2);
		return(((refs1 > 2) && (refs2 > 2)));
	}

	protected Edge findFirstSafeEdge(Board board)
	{
		Edge retval = null;

		for (Object obj : board.getEdges()) {
			Edge edge = (Edge)obj;

			if (Util.isTaken(board, edge)) {
				continue;
			}
			if (isTrulySafe(edge)) {
				retval = edge;
				break;
			}
		}
		logger.debug((retval != null)
			? "found safe edge"
			: "no edge considered safe...");
		return(retval);
	}

	protected Edge findEdgeWithHighestScore(Board board)
	{
		Edge retval = null;
		int score = 0;
		int low = 0;

		for (Object obj : board.getEdges()) {
			Edge edge = (Edge)obj;
			int refs1 = edge.getPoint1().numReferences();
			int refs2 = edge.getPoint2().numReferences();
			int newscore = (refs1 + refs2);
			
			if (Util.isTaken(board, edge)) {
				continue;
			}
			else if (newscore >= score) {
				if ((refs1 > low) && (refs2 > low)) {
					low = Math.min(refs1, refs2);
					score = newscore;
					retval = edge;
				}
			}
		}
		return(retval);
	}
}
