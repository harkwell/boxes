// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Board;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirstAvailableChooser implements Chooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		FirstAvailableChooser.class);

	public FirstAvailableChooser() { }

	public Edge choose(Board board)
	{
		Edge retval = null;

		for (Object obj : board.getEdges()) {
			Edge edge = (Edge)obj;

			if (Util.isTaken(board, edge)) {
				continue;
			}
			retval = edge;
			break;
		}
		logger.debug("first available edge: ("+retval+")");
		return(retval);
	}
}
