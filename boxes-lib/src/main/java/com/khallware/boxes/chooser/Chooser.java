// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Board;

public interface Chooser
{
	public Edge choose(Board board);
}
