// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Square;
import com.khallware.boxes.GameBoard;
import com.khallware.boxes.Board;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * This chooser tries to find first, any final edges and then the safest
 * edge (those with the highest point values).
 */
public class OptimalChooser extends ModerateChooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		OptimalChooser.class);
	private Board board = null;

	public OptimalChooser()
	{
		logger.debug("OptimalChooser() constructor called");
	}

	@Override
	public Edge choose(Board board)
	{
		Edge retval = null;
		this.board = board;
		retval = super.choose(board);
		return(retval);
	}

	@Override
	protected boolean isTrulySafe(Edge edge)
	{
		boolean retval = true;

		for (Square square : Util.squaresHavingEdge((GameBoard)board,
				edge)) {
			logger.debug("is edge ("+edge+") a third edge in "
				+"square ("+square+")");
			retval &= (square.getTakenEdges().size() < 2);
		}
		logger.debug((retval) ? "safe!" : "not suggesting third edge");
		return(retval);
	}
}
