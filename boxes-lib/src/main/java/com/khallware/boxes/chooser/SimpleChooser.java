// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes.chooser;

import com.khallware.boxes.Edge;
import com.khallware.boxes.Board;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleChooser extends RandomChooser
{
	public static final Logger logger = LoggerFactory.getLogger(
		SimpleChooser.class);

	public SimpleChooser() { }

	public Edge choose(Board board)
	{
		Edge retval = null;
		List<Edge> finalEdges = Util.findFinalEdges(board);

		if (finalEdges.size() > 0) {
			retval = finalEdges.get(0);
		}
		else {
			retval = super.choose(board);
		}
		logger.debug("next simple edge: ("+retval+")");
		return(retval);
	}
}
