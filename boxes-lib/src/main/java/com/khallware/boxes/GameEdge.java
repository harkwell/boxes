// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameEdge implements Edge<Point>
{
	public static int sequence = 0;
	private static final Logger logger =
		LoggerFactory.getLogger(GameEdge.class);

	protected Point point1 = null;
	protected Point point2 = null;
	protected int id = (sequence++);
	protected List<EdgeHandler> callbacks = new ArrayList<>();

	public GameEdge(Point point1, Point point2)
	{
		if (point1.getId() < point2.getId()) {
			this.point1 = point1;
			this.point2 = point2;
		}
		else {
			this.point1 = point2;
			this.point2 = point1;
		}
	}

	public Point getPoint1()
	{
		return(point1);
	}

	public Point getPoint2()
	{
		return(point2);
	}

	public boolean take()
	{
		getPoint1().takeReference();
		getPoint2().takeReference();
		notifyCallbacks();
		return(true);
	}

	public void register(EdgeHandler handler)
	{
		callbacks.add(handler);
	}

	public boolean hasHandlers()
	{
		return((callbacks.size() > 0));
	}

	protected void notifyCallbacks()
	{
		for (EdgeHandler handler : callbacks) {
			handler.handle(this);
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		return(obj != null && this != null
			&& obj.hashCode() == this.hashCode());
	}

	@Override
	public int hashCode()
	{
		return(id);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(this.getClass().getSimpleName()+" ")
			.append("hashCode=\""+hashCode()+"\" ")
			.append("point1=("+getPoint1()+") ")
			.append("point2=("+getPoint2()+"), ")
			.append("edge_callbacks="+callbacks.size())
			.toString());
	}
}
