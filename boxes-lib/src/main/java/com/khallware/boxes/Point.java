// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

public interface Point
{
	public static final int MAX_POINT_ID = 1000000;
	public int getId();
	public int numReferences();
	public boolean hasHandlers();
	public boolean takeReference();
	public boolean releaseReference();
	public boolean hasAvailableReference();
	public void register(PointHandler handler);
}
