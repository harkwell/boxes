// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameSquare implements Square<Point, Edge, Player>
{
	private static final Logger logger =
		LoggerFactory.getLogger(GameSquare.class);
	public static int sequence = 0;

	protected final List<Edge> taken = new ArrayList<>();
	protected List<Edge> edges = new ArrayList<>();
	protected Player owner = null;
	protected int id = (sequence++);

	public GameSquare(List<Edge> edges) throws IllegalArgumentException
	{
		if (edges == null || edges.size() != 4) {
			throw new IllegalArgumentException("must have 4 edges");
		}
		EdgeHandler handler = new EdgeHandler() {
			public void handle(Edge edge) {
				logger.debug("handle edge ("+edge+")");

				if (!taken.contains(edge)) {
					taken.add(edge);
				}
			}
		};
		for (Edge edge : edges) {
			edge.register(handler);
			this.edges.add(edge);
		}
	}

	public List<Point> getPoints()
	{
		List<Point> retval = new ArrayList<>();

		for (Edge edge : getEdges()) {
			Point point = edge.getPoint1();

			if (!retval.contains(point)) {
				retval.add(point);
			}
			point = edge.getPoint2();

			if (!retval.contains(point)) {
				retval.add(point);
			}
		}
		return(retval);
	}

	public List<Edge> getEdges()
	{
		return(edges);
	}

	public List<Edge> getTakenEdges()
	{
		return(taken);
	}

	public boolean isComplete()
	{
		return(getTakenEdges().size() >= 4);
	}

	public Player getOwner()
	{
		return(owner);
	}

	public void setOwner(Player owner)
	{
		this.owner = owner;
	}

	@Override
	public boolean equals(Object obj)
	{
		return(obj != null && this != null
			&& obj.hashCode() == this.hashCode());
	}

	@Override
	public int hashCode()
	{
		return(id);
	}

	@Override
	public String toString()
	{
		List<Integer> points = new ArrayList<>();

		for (Point point : getPoints()) {
			points.add(point.getId());
		}
		return(new StringBuilder()
			.append(this.getClass().getSimpleName()+" ")
			.append("hashCode="+hashCode()+" ")
			.append("takenEdges="+getTakenEdges().size()+" ")
			.append("points=("+points+")")
			.toString());
	}
}
