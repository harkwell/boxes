// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.chooser.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameBoard implements Board<Point, Square, Edge, Player>
{
	public static boolean enforceNumPlayers = false;
	private static final Logger logger =
		LoggerFactory.getLogger(GameBoard.class);

	protected Point[][] grid = null;
	protected List<Square> squares = new ArrayList<>();
	protected List<Player> players = null;

	public GameBoard(int width, int height, List<Player> players)
			throws GameException
	{
		int idx = 0;
		this.players = players;
		grid = new Point[width+1][height+1];

		if (enforceNumPlayers && (players == null
				|| players.size() <= 1)) {
			throw new GameException("too few players");
		}
		for (int row=0; row<height+1; row++) {
			for (int col=0; col<width+1; col++) {
				int maxRefs = 2;

				if (row > 0 && row < height) {
					maxRefs++;
				}
				if (col > 0 && col < width) {
					maxRefs++;
				}
				grid[row][col] = newPoint(idx++, maxRefs);

				if (row > 0 && col > 0) {
					getSquares().add(newSquare(
						grid[row-1][col-1],
						grid[row-1][col],
						grid[row][col-1],
						grid[row][col]));

				}
			}
		}
	}

	public Point newPoint(int id, int maxRefs)
	{
		return(new GamePoint(id, maxRefs));
	}

	public Square newSquare(Point p1, Point p2, Point p3, Point p4)
	{
		List<Edge> list = Util.createOrFindEdges(this, p1, p2, p3, p4);
		return(new GameSquare(list));
	}

	public boolean move(Player player, Edge edge) throws GameException
	{
		if ((edge = Util.findEdge(edge, getSquares())) == null) {
			throw new GameException("invalid edge ("+edge+")");
		}
		else if (Util.isTaken(this, edge)) {
			throw new GameException("edge has already been taken "
				+"("+edge+")");
		}
		logger.debug("take edge ("+edge+")");
		return(edge.take());
	}

	public boolean move(Player player, Point p1, Point p2)
			throws GameException
	{
		boolean retval = false;

		if (p1.hasAvailableReference() && p2.hasAvailableReference()) {
			if (!Util.isEdge(this, p1, p2)) {
				throw new GameException("points do not form "
					+"an edge ("+p1+", "+p2+")");
			}
			else {
				retval = move(player, new GameEdge(p1, p2));
			}
		}
		else {
			throw new GameException("cannot make an edge with "
					+"unavailable point ("+p1+", "+p2+")");
		}
		return(retval);
	}

	public List<Player> getPlayers()
	{
		return(players);
	}

	public List<Edge> getEdges()
	{
		List<Edge> retval = new ArrayList<>();

		for (Square square : getSquares()) {
			retval.addAll(square.getEdges());
		}
		return(retval);
	}

	public List<Square> newlyCompletedSquares()
	{
		List<Square> retval = new ArrayList<>();

		for (Square square : getSquares()) {
			if (square.isComplete() && square.getOwner() == null) {
				logger.debug("found completed square: "+square);
				retval.add(square);
			}
		}
		return(retval);
	}

	public Point getPoint(int id)
	{
		Point retval = null;
		Point point = null;

		LOOP: for (int row=0; row < grid.length; row++) {
			for (int col=0; col < grid[row].length; col++) {
				point = grid[row][col];

				if (point.getId() == id) {
					retval = point;
					break LOOP;
				}
			}
		}
		return(retval);
	}

	public List<Point> getPoints()
	{
		List<Point> retval = new ArrayList<>();

		for (Point[] row : getPointGrid()) {
			for (Point point : row) {
				retval.add(point);
			}
		}
		return(retval);
	}

	public Point[][] getPointGrid()
	{
		return(grid);
	}

	public List<Square> getSquares()
	{
		return(squares);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(this.getClass().getSimpleName()+" ")
			.append(getSquares())
			.toString());
	}
}
