// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.ArrayList;
import java.util.List;

public class GamePlayer implements Player<Square>
{
	public static final int UNKNOWN = -1;

	protected int id = UNKNOWN;
	protected String name = "";
	protected List<Square> squares = null;

	public GamePlayer(int id, String name)
	{
		this.id = id;
		this.name = name;
		squares = new ArrayList<>();
	}

	public int getId()
	{
		return(id);
	}

	public String getName()
	{
		return(name);
	}

	public List<Square> getSquares()
	{
		return(squares);
	}

	@Override
	public boolean equals(Object obj)
	{
		return(obj != null && this != null
			&& obj.hashCode() == this.hashCode());
	}

	@Override
	public int hashCode()
	{
		return(id);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(this.getClass().getSimpleName()+" ")
			.append("id=\""+getId()+"\" ")
			.append("name=\""+getName()+"\" ")
			.append("numSquares=\""+getSquares().size()+"\"")
			.toString());
	}
}
