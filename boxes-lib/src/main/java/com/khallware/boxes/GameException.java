// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

public class GameException extends Exception
{
	private static final long serialVersionUID = 0x0001L;

	public GameException()
	{
	}

	public GameException(String message)
	{
		super(message);
	}

	public GameException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public GameException(Throwable cause)
	{
		super(cause);
	}
}
