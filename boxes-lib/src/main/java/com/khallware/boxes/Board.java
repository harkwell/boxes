// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.List;
import java.util.Map;

public interface Board<P1 extends Point, S extends Square, E extends Edge,
		P2 extends Player>
{
	public P1 getPoint(int id);
	public P1[][] getPointGrid();
	public List<E> getEdges();
	public List<S> getSquares();
	public List<S> newlyCompletedSquares();
	public P1 newPoint(int id, int maxRefs);
	public S newSquare(Point p1, Point p2, Point p3, Point p4);
	public boolean move(P2 player, E edge) throws GameException;
}
