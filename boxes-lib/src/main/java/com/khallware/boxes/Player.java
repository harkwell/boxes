// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.List;

public interface Player<S extends Square>
{
	public int getId();
	public String getName();
	public List<S> getSquares();
}
