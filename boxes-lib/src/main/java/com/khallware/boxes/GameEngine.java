// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.chooser.Chooser;
import com.khallware.boxes.chooser.Util;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameEngine implements Engine<Player, Board, Chooser>
{
	private static final Logger logger =
		LoggerFactory.getLogger(GameEngine.class);

	protected Map<? extends Player, ? extends Chooser> map = null;
	protected List<Player> players = new ArrayList<>();
	protected Board board = null;
	protected boolean done = false;
	protected int moves = 0;

	public void beforeFirstMove()
	{
		moves = 0;
	}

	public void beforeMove(Player player)
	{
		logger.debug("player "+player+"'s turn...");
	}

	public void afterMove(Player player, Edge edge)
	{
	}

	public void afterScore(boolean tookSquare)
	{
		logger.info(""+board);
	}

	public boolean movesLeft()
	{
		return((!done && !Util.isGameOver((GameBoard)board)));
	}

	public Player determineWinner()
	{
		Player retval = null;

		for (Player player : players) {
			if (retval == null || player.getSquares().size()
					> retval.getSquares().size()) {
				retval = player;
			}
		}
		return(retval);
	}

	public void revertMove(Player player, Edge edge)
	{
		logger.debug("reverting move");
	}

	public void afterLastMove()
	{
		resetPlay();
	}

	/**
	 * Play the game, return the winner.
	 */
	public Player playGame(Map<? extends Player, ? extends Chooser> map,
			Board board) throws GameException
	{
		Player retval = null;
		Player player = null;
		Chooser chooser = null;
		boolean goAgain = false;
		Edge edge = null;

		this.map = map;
		this.board = board;
		players.clear();
		players.addAll(map.keySet());
		logger.info("playing game with "+players.size()+" players");
		beforeFirstMove();

		LOOP: while (!done) {
			player = players.get((moves % players.size()));
			beforeMove(player);
			chooser = map.get(player);
			goAgain = false;
			do {
				if ((edge = chooser.choose(board)) == null) {
					done = true;
					logger.info("game over");
					break LOOP;
				}
				try {
					done = (!board.move(player, edge));
					afterMove(player, edge);
					goAgain = Util.increaseScore(board,
						player);
					afterScore(goAgain);

					if (!goAgain) {
						moves++;
					}
				}
				catch (GameException e) {
					// edge already taken?
					logger.error(""+e, e);
					revertMove(player, edge);
					done = false;
				}
			}
			while (goAgain && movesLeft());
			done |= (!movesLeft());
		}
		retval = determineWinner();
		logger.info(""+retval+" wins");
		afterLastMove();
		return(retval);
	}

	protected void resetPlay()
	{
		for (Player player : players) {
			player.getSquares().clear();
		}
		done = false;
	}
}
