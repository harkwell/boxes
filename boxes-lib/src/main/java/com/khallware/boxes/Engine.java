// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import com.khallware.boxes.chooser.Chooser;
import java.util.Map;

public interface Engine<P extends Player, B extends Board, C extends Chooser>
{
	public P playGame(Map<? extends Player, ? extends Chooser> map, B board)
		throws GameException;
	public void beforeFirstMove();
	public void beforeMove(Player player);
	public void afterMove(Player player, Edge edge);
	public void revertMove(Player player, Edge edge);
	public void afterScore(boolean tookSquare);
	public boolean movesLeft();
	public Player determineWinner();
	public void afterLastMove();
}
