// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.List;
import java.util.ArrayList;

public class GamePoint implements Point
{
	public static final int UNKNOWN = -1;
	public static final int MAX_REFERENCES = 4;

	protected List<PointHandler> callbacks = null;
	protected int id = UNKNOWN;
	protected int numReferences = 0;

	public GamePoint(int id, int numReferences)
	{
		this.id = id;
		this.numReferences = numReferences;
		callbacks = new ArrayList<>();
	}

	public int getId()
	{
		return(id);
	}

	public void register(PointHandler handler)
	{
		if (!callbacks.contains(handler)) {
			callbacks.add(handler);
		}
	}

	public boolean takeReference()
	{
		boolean retval = false;

		if (numReferences > 0) {
			numReferences--;
			notifyCallbacks();
			retval = true;
		}
		return(retval);
	}

	public boolean releaseReference()
	{
		boolean retval = false;

		if (numReferences < MAX_REFERENCES) {
			numReferences++;
			retval = true;
		}
		return(retval);
	}

	public int numReferences()
	{
		return(numReferences);
	}

	public boolean hasAvailableReference()
	{
		return(numReferences() > 0);
	}

	public boolean hasHandlers()
	{
		return((callbacks.size() > 0));
	}

	protected void notifyCallbacks()
	{
		for (PointHandler handler : callbacks) {
			handler.handle(this);
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		return(obj != null && this != null
			&& obj.hashCode() == this.hashCode());
	}

	@Override
	public int hashCode()
	{
		return(id);
	}

	@Override
	public String toString()
	{
		return(new StringBuilder()
			.append(this.getClass().getSimpleName()+" ")
			.append("id="+getId()+" ")
			.append("numRefs="+numReferences()+" ")
			.append("num_callbacks="+callbacks.size())
			.toString());
	}
}
