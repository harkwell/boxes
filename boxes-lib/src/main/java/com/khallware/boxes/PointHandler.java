// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

public interface PointHandler
{
	public void handle(Point point);
}
