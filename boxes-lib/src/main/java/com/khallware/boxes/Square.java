// copyright Kevin D.Hall 2013-2016

package com.khallware.boxes;

import java.util.List;

public interface Square<P1 extends Point, E extends Edge, P2 extends Player>
{
	public P2 getOwner();
	public boolean isComplete();
	public void setOwner(P2 owner);
	public List<E> getTakenEdges();
	public List<P1> getPoints();
	public List<E> getEdges();
}
